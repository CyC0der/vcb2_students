<?php
/**
 *
 * PHP version 5.3
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @author     korosh raoufi <k2_4u@yahoo.com>
 * @version    Release: v1.0
 * @link       http://k2-4u.com
 *
 */

define('ER_DB',1);
define('ER_PAYMENT',2);
define('ER_ENGINE',3);
define('ER_ATTACK',4);
define('ER_NOSY',5);
define('ER_PHP',6);
  
const IMPORTANT = 1;
const TECHNICAL = 2;
const SECURITY = 3;
const MONITOR = 4;

 
 /*
if(strpos($_SERVER['HTTP_HOST'],'localhost') === FALSE) {
	die('invalid licence<br> please call  +989189316778 Koroush Raoufi');
}
*/

/* disable error reporting for securty */
//error_reporting(0);

define('SYSTEM_PATH', __DIR__);

define('SITE_ROOT','panel');

 /** load config file **/
require_once SYSTEM_PATH . '/core/config.php';

/* start sessions */
session_start();

$registry = new engine\registry;

/* open database and add to the registry */
$registry->MySQL = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_NAME);

/* load template */
$registry->template = new library\template();

/* load router */
$registry->router = new engine\router($registry);

/* check database connection */
if ($registry->MySQL->connect_error)
	throw new Exception('Connect Error (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error);
	
if (!$registry->MySQL->set_charset("utf8"))
	throw new Exception("Error loading character set utf8: " . $mysqli->error);

/* set access to Administrator if not direct to login Controller */
$registry->router->setAccess(1, 'login');

/* load controller */
$registry->router->loader();

 



?>