
		function sync() {	
			try {
				$.getJSON('index.php?r=index/sync&pack_id='+packID, function(data,stauts){
				
					//("success", "notmodified", "error", "timeout", or "parsererror").
					// if its a int
					if(stauts=='success') {
						
						packID = 1;
						
						reputation = data.reputation;
						
						medals = data.medals;
						
						timeline = data.timeline;
						
						update() ;
		
						//load.hide();
					}
					
				}).fail(function() {
					//load.hide();
				});
			} catch(err) {
				//load.hide();
			};
		}
		
		function update() {
			
			$('.timeline').html('');
			
			for(i in timeline) {
				$('.timeline').append(tmpl("tmpl-timeline",timeline[i]));
			}
	
			$('.reputation').html(tmpl("tmpl-reputation",{rep:reputation}));
			$('.medals').html(tmpl("tmpl-medals",{medals:medals}));
		}

		update();
		
		
        $('.icheck').iCheck({
            checkboxClass: 'icheckbox_square-blue checkbox',
            radioClass: 'iradio_square-blue'
        });
		
        for (i = 10 - current_session; i < 10; i++)
            $('.student-session .btn:eq(' + i + ')').removeClass('disabled');

        $('.student-session .btn:not(.disabled)').click(function() {

			current_session = 10 - $(this).index();
		
			var load = new loading($('.session-box'));
			
			load.show();
			
			try {
				$.getJSON('index.php?r=index/session&session='+current_session+'&pack_id='+packID, function(data,stauts){
				
					//("success", "notmodified", "error", "timeout", or "parsererror").
					// if its a int
					if(stauts=='success') {

						if(typeof data.comment!= 'undefined') {
							
							$('.commentMode').show();
							$('.taskMode').hide();
							
							$('.task-comment').find('article').remove();
							
							var url = "index.php?r=index/download&pack_id="+packID+"&session="+current_session+"&file=";
							
							$('#first_jpg')[0].src = url + 'first_jpg';
							$('#first_pdf')[0].href = url + 'first_pdf';
							$('#first_answers')[0].href = url + 'first_answers';
							$('#second_jpg')[0].src = url + 'second_jpg';
							$('#second_pdf')[0].href = url + 'second_pdf';
							$('#second_answers')[0].href = url + 'second_answers';
							$('#task_jpg')[0].src = url + 'task_jpg';
							$('#task_pdf')[0].href = url + 'task_pdf';
							$('#task_answers')[0].href = url + 'task_answers';

							for(i in data.comment) {
								if(data.comment[i].is_owner == 1)
									$('.task-comment').prepend(tmpl("tmpl-comment-owner", data.comment[i]));
								else
									$('.task-comment').prepend(tmpl("tmpl-comment-user", data.comment[i]));
								
							}
						}
						
						if(data.showTask) {
							$('.commentMode').hide();
							$('.taskMode').show();
						};
					
						/*
							fetch type  (addTask & Not)
								Else
									1. comments
									2.
						*/
						
						load.hide();
					}
					
				}).fail(function() {
					load.hide();
				});
			} catch(err) {
				load.hide();
			};
			
			
            $('.student-session .btn:not(.disabled)').removeClass('btn-primary active');
            $(this).addClass('btn-primary active');

        });
		
		
		 $('.student-session .btn:eq(' + (10 - current_session) + ')').addClass('btn-primary active').click();
		
		var studentEditInfo = $('[name="studentEditInfo"]').ft({
			validate: true,
			closeModal: true,
			bindValues: function(f) {   
				return {
					schools_id: f.find('[name="schools_id"]').val(),
					address: f.find('[name="address"]').val(),
					postal_code: f.find('[name="postal_code"]').val(),
					tell: f.find('[name="tell"]').val(),
					cell: f.find('[name="cell"]').val()
				};
			},
		});
		
		$('[href="#tabEditResellerModirator"]').click(function() {
			studentEditInfo.fetchData('?r=index/fetch&name=studentEditInfo');
		});
	
		$('[name="studentChangePassword"]').ft({
			validate: true,
			resetSuccess: true,
			bindValues: function(f) {   
				return {

					password: f.find('[name="password"]').val(),
					new_password: f.find('[name="new_password"]').val(),
					repeat_password: f.find('[name="repeat_password"]').val()
				};
			}
		});
		
		$('[name="studentAddPoints"]').ft({
			done:function(d,res) {
				if(res) {
					$('.student-session .btn:eq(' + (10 - current_session) + ')').click();
					$("#graph_img").attr("src", "?r=graph&timestamp=" + new Date().getTime());
					 sync();
				}
			},
			validate: true,
			resetSuccess: true,
			bindValues: function(f) {
				return {
					pack_id: packID,
					session: current_session,
					taskResult0: f.find('[name="taskResult0"]:checked').val(),
					taskResult1: f.find('[name="taskResult1"]:checked').val(),
					taskResult2: f.find('[name="taskResult2"]:checked').val(),
					taskResult3: f.find('[name="taskResult3"]:checked').val(),
					taskResult4: f.find('[name="taskResult4"]:checked').val(),
					taskResult5: f.find('[name="taskResult5"]:checked').val(),
					taskResult6: f.find('[name="taskResult6"]:checked').val(),
					comment: f.find('[name="comment"]').val()
				};
			}
		});
		
		$('[name="addComment"]').ft({
			done:function(d,res) {
				if(res) {
					$('.student-session .btn:eq(' + (10 - current_session) + ')').click();
					
				}
			},
			validate: true,
			resetSuccess: true,
			bindValues: function(f) {
				return {
					pack_id: packID,
					session: current_session,
					text: f.find('[name="text"]').val()
				};
			}
		});
		
		$('[name="studentRegister"]').ft({
			done:function(d,res) {
				if(res)
					window.location.reload();
			},
			validate:true,
			closeModal: true,
			bindValues:function(f) { 
			
				var birthday_day = f.find('[name="birthday_day"]').val();
				var birthday_month = f.find('[name="birthday_month"]').val();
				var birthday_year = f.find('[name="birthday_year"]').val();
					
				return {
					firstname:f.find('[name="firstname"]').val(),
					lastname:f.find('[name="lastname"]').val(),
					birthday:birthday_year+'-'+birthday_month+'-'+birthday_day,
					tell:f.find('[name="tell"]').val(),
					//city_id:f.find('[name="city_id"]').val(),
					//town_id:f.find('[name="town_id"]').val(),
					//area_id:f.find('[name="area_id"]').val(),
					//zone_id:f.find('[name="zone_id"]').val(),
					postal_code:f.find('[name="postal_code"]').val(),
					cell:f.find('[name="cell"]').val(),
					schools_id:f.find('[name="schools_id"]').val()
				};
			}
	}).fetchData('?r=index/fetch&name=studentRegister');
	
		$(function() {
			$('.form-tooltip [type="text"]').tooltip({
				trigger: "focus"
			});
			$('.form-tooltip .form-control').tooltip({
				trigger: "focus"
			});
		})

		$('[name="postal_code"]').attr({
			"pattern": "^([0-9]{10})$",
			"data-parsley-pattern-message": "کدپستی را اشتباه وارد کرده اید"
		});

		$('[name="cell"]').attr({
			"pattern": "^(09[0-9]{9})$",
			"data-parsley-pattern-message": "شماره موبایل را اشتباه وارد کرده اید"
		});

		$('[name="tell"]').attr({
			"pattern": "^([0-9]{8})$",
			"data-parsley-pattern-message": "شماره تلفن را اشتباه وارد کرده اید"
		});
		
		$('[name="cell"]').attr({
			'data-parsley-remote':'',
			'data-parsley-remote-validator':'dublicate'
		});
		
		//$('[name="address"]').attr('data-parsley-persian','fa09');
		$('[name="view"]').attr('data-parsley-persian','fa09');
		$('[name="firstname"]').attr('data-parsley-persian','fa');
		$('[name="lastname"]').attr('data-parsley-persian','fa');
	
		$('[name="tell"]').attr({
			"pattern":"^([0-9]{8})$",
			"data-parsley-pattern-message":"تلفن را اشتباه وارد کرده اید ، کد شهر را وارد نکنید"
		});

		$('[name="postal_code"]').attr({
			"pattern":"^([0-9]{10})$",
			"data-parsley-pattern-message":"کدپستی را اشتباه وارد کرده اید"
		});
		
	
		$('[name="cell"]').attr({
			"pattern":"^(09[0-9]{9})$",
			"data-parsley-pattern-message":"شماره موبایل را اشتباه وارد کرده اید"
		});
		
		$('[name="cell"]').attr({
			'data-parsley-remote':'',
			'data-parsley-remote-validator':'dublicate'
		});
		
