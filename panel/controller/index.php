<?php
/**
 *
 * PHP version 5.3
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @author     korosh raoufi <k2_4u@yahoo.com>
 * @version    Release: v1.0
 * @link       http://k2-4u.com
 *
 */
 
 namespace controller;
 
Class index Extends \engine\Controller
{
	public function index() {
		
		require_once('core/library/jdatetime.class.php');
		
		
		$packsID = 1;
		
		$this->template->load('index');

		$studentRow  =  $this->getStudentRow();
		
		list($current_session) = $this->MySQL->query('SELECT `current_session`+0 FROM `premium_packs` WHERE(`premium_packs_id`='.$packsID.')')->fetch_array();
		
		$qy = $this->MySQL->query('SELECT  * 
			FROM `students_timeline` 
			INNER JOIN `timeline_event`
			USING(`timeline_event_id`)
			WHERE(`students_id`="'.$_SESSION[studentUserID].'") ORDER BY students_timeline_id DESC LIMIT 0,8;
		');
		
		$timeline = array();
		while($row = $qy->fetch_assoc()) {
			
			$row[text] = strtr($row[text],array(
				'{var1}'=> $row[var1],
				'{var2}'=>  $row[var2],
				'{var3}'=>  $row[var3]
			));

			$date = new \jDateTime(true, true, 'Asia/Tehran');

			$timeline[] = array(
				'date' =>  $date->date("j F",  strtotime($row[date]), false, true, 'Asia/Tehran'),
				'text' => $row[text],
				'title'=> $row[title],
				'icon'=> $row[icon]
			);
		}
		
		//$students_medal = $this->MySQL->query('SELECT medals_id,count FROM students_medals INNER JOIN medals USING(medals_id) WHERE(students_id="'.$_SESSION[studentUserID].'");')->fetch_all();
		
		$qy = $this->MySQL->query('SELECT medals_id,count FROM students_medals INNER JOIN medals USING(medals_id) WHERE(students_id="'.$_SESSION[studentUserID].'");');
		
		$students_medal = array();
		while($row=$qy->fetch_row())
			$students_medal[] = $row;
		
		$this->template->assign(array(
			'current_session' => $current_session,
			'timeline' => json_encode($timeline),// ,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT),
			'show_register' => ($studentRow[first_register]==0 ? 'true' : 'false'),
			'student_name' => $studentRow[firstname] . ' ' . $studentRow[lastname],
			'reputation' => $studentRow[reputation],
			'medals' => json_encode($students_medal),
			'static_path' => 'panel/static'
		));
		
		$this->template->exec();
	}
	
	public function sync(){
		
		require_once('core/library/jdatetime.class.php');
				
		$qy = $this->MySQL->query('SELECT medals_id,count FROM students_medals INNER JOIN medals USING(medals_id) WHERE(students_id="'.$_SESSION[studentUserID].'");');
		
		$students_medal = array();
		while($row=$qy->fetch_row())
			$students_medal[] = $row;
		
		$studentRow  =  $this->getStudentRow();
		
		$qy = $this->MySQL->query('SELECT *
			FROM `students_timeline` 
			INNER JOIN `timeline_event`
			USING(`timeline_event_id`)
			WHERE(`students_id`="'.$_SESSION[studentUserID].'") ORDER BY students_timeline_id DESC LIMIT 0,8;
		');
		
		$timeline = array();
		while($row = $qy->fetch_assoc()) {
			
			$row[text] = strtr($row[text],array(
				'{var1}'=> $row[var1],
				'{var2}'=>  $row[var2],
				'{var3}'=>  $row[var3]
			));
			
			$date = new \jDateTime(true, true, 'Asia/Tehran');
			
			$timeline[] = array(
				'date' =>  $date->date("j F",  strtotime($row[date]), false, true, 'Asia/Tehran'),
				'text' => $row[text],
				'title'=> $row[title],
				'icon'=> $row[icon]
			);
		}
		
		echo json_encode(array(
			'timeline' => $timeline,// ,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT),
			'reputation' => $studentRow[reputation],
			'medals' => $students_medal
		));
	}
		
	private function getFirstRegister(){
		
		list($first_register) = $this->MySQL->query('SELECT 
			`first_register` 
			FROM `students` 
			WHERE(`students_id`="'.$_SESSION[studentUserID].'")'
		)->fetch_array();
		
		return $first_register;
		
	}
	
	private function getStudentRow() {
		
		return $this->MySQL->query('SELECT 
			* 
			FROM `students` 
			WHERE(`students_id`="'.$_SESSION[studentUserID].'")'
		)->fetch_assoc();
	}
	
	private function updateReputation(){
		$this->MySQL->query('UPDATE `students` SET
			`reputation` = (
				SELECT SUM(reputation) 
				FROM `students_reputation` 
				WHERE(`students_id`="'.$_SESSION[studentUserID].'")
			) WHERE(`students_id`="'.$_SESSION[studentUserID].'");'
		);
	}
	
	public function logout() {
		
		session_destroy();
		header('location: index.php?r=login');
	}
	
	public function remoteCheck() {
		
		sleep(2);
		
		//for showing success result , remove comments of this part
		echo '{"status":1}';
		exit;

		if(isset($_GET[tell])) {
				echo '{"status":0,"message":"شماره تلفن وارد شده قبلا به وسیله شخص دیگری ثبت شد"}';
				exit;
		}

		if(isset($_GET[email])) {
				echo '{"status":0,"message":"ایمیل وارد شده قبلا در بانک ثبت شده"}';
				exit;
		}

		if(isset($_GET[cell])) {
				echo '{"status":0,"message":"شماره موبایل وارد شده قبلا در بانک ثبت شده"}';
				exit;
		}

		if(isset($_GET[postal_code])) {
				echo '{"status":0,"message":"کد پستی وارد شده قبلا در بانک ثبت شده"}';
				exit;
		}

	}

	public function session() {
	
		require_once('core/library/jdatetime.class.php');
				
		$out = array();
		
		/* check if task added before */
		$check_if_task_added = $this->MySQL->query('SELECT
			`premium_students_sessions_id` 
		FROM  `premium_students_sessions` 
		WHERE(
			`session`="'.$_GET[session].'" AND 
			`premium_packs_id`="'.$_GET[pack_id].'" AND 
			students_id="'.$_SESSION[studentUserID].'"
		);'
		)->num_rows;
		
		if(	$check_if_task_added == 1) {
			
			$out[showTask] = 0;
			
			$qy = $this->MySQL->query('SELECT * FROM `premium_comments` 
				INNER JOIN `premium_packs_students` USING(`premium_packs_students_id`) 
				WHERE(
					`session`="'.$_GET[session].'" AND
					`students_id`="'.$_SESSION[studentUserID].'"
				) 
				ORDER BY `premium_commets_id` DESC;'
			);
			
			$out[comment] = array();
			while($comment = $qy->fetch_assoc()) {
				
				$comment[picture] = 'panel/static/img/student-profile.png';
				
				$date = new \jDateTime(true, true, 'Asia/Tehran');

				$comment['date'] = $date->date("l j F Y H:i", strtotime($comment[date]), false, true, 'Asia/Tehran');
				
				$out[comment][] = $comment;
				
			}
			
		} else{
			$out[showTask] = 1;
		}
		
	
		echo json_encode($out);
	}
	
	public function fetch() {
		
		switch($_GET[name]) {
			
			case 'studentEditInfo':
			
				echo json_encode($this->MySQL->query('SELECT 
					`tell`,
					`postal_code`,
					`cell`,
					`schools_id`,
					`address`
					FROM  `students` 
					WHERE(`students_id`="'.$_SESSION[studentUserID].'");'
				)->fetch_assoc());
				
			break;
			
			case 'studentRegister':
			
			/*		این فیلد ها فعلاً حذف شده
			
					`city_id`,
					`town_id`,
					`area_id`,
					`zone_id`,*/
					
				$info = $this->MySQL->query('SELECT 
					`firstname`,
					`lastname`,
					`birthday`,
					`tell`,
					`postal_code`,
					`cell`,
					`schools_id`,
					`birthday`
					FROM  `students` 
					WHERE(`students_id`="'.$_SESSION[studentUserID].'");'
				)->fetch_assoc();
				
				$birthday = explode('-',$info[birthday]);
				
				unset($info[birthday]);
				
				$info[birthday_year] = $birthday[0];
				$info[birthday_month] = (int)$birthday[1];
				$info[birthday_day] = (int)$birthday[2];
				
				echo json_encode($info);
				
			break;
				
			default:
				echo 'No FORM selected!';
		}
	}
	
	public function forms() {

		switch($_GET[name]) {
			
			case 'studentRegister':
				$this->studentRegister();
			break;
			
			case 'studentChangePassword':
				$this->studentChangePassword();
			break;
			
			case 'studentEditInfo':
				$this->studentEditInfo();
			break;

			case 'studentAddPoints':
				$this->studentAddPoints();
			break;
			
			case 'addComment':
				$this->addComment();
			break;
			
			default:
				echo 'No FORM selected!';
		}
		
	}
	
	public function download(){
		
		if(!preg_match('/^([0-9]{1,9})$/i',$_GET[pack_id]) ||
			!preg_match('/^([0-9]{1,2})$/i',$_GET[session])
		) {
			echo '<b style="text-align:center;font-family:tahoma">درخواست شما برای دریافت فایل مجاز نیست <br>'.
			'<a href="javascript:window.history.back()">[ برگشت ]</a></b>';
			exit;
		}
			
		$qy = $this->MySQL->query('SELECT `premium_packs_id` 
			FROM `premium_packs_students` 
			INNER JOIN `premium_packs`
			USING(`premium_packs_id`)
			WHERE(`premium_packs`.`premium_packs_id`="'.$_GET[pack_id].'" AND
			`students_id`="'.$_SESSION[studentUserID].'");'
		);
		
		if(!$qy || $qy->num_rows == 0) {	
			echo '<b style="text-align:center;font-family:tahoma">درخواست شما برای دریافت فایل مجاز نیست <br>'.
			'<a href="javascript:window.history.back()">[ برگشت ]</a></b>';
			exit;
		}
			
		switch($_GET[file]){	
			case 'first_jpg': $type = 'first_jpg'; break;
			case 'first_pdf': $type = 'first_pdf'; break;
			case 'first_answers': $type = 'first_answers'; break;
			case 'second_jpg': $type = 'second_jpg'; break;
			case 'second_pdf':$type = 'second_pdf'; break;
			case 'second_answers': $type = 'second_answers'; break;
			case 'task_jpg': $type = 'task_jpg'; break;
			case 'task_pdf': $type = 'task_pdf'; break;
			case 'task_answers': $type = 'task_answers'; break;
			default:
				echo '<b style="text-align:center;font-family:tahoma">درخواست شما برای دریافت فایل مجاز نیست <br><a href="javascript:window.history.back()">[ برگشت ]</a></b>';
				exit;
			break;
		}
					
		$file = $this->MySQL->query('SELECT `'.$type.'` as file,`active` FROM `premium_files` WHERE(`premium_packs_id`="'.$_GET[pack_id].'" AND `session`="'.$_GET[session].'");')->fetch_assoc();
		
		
		if(!$file) {
			header('Content-Type: image/jpeg');
			//echo ' image row is not exits';
			echo file_get_contents('panel/static/img/sheet-not-exists.png');
			exit;
		}
		
				
		if($file['active']==0) {
			header('Content-Type: image/jpeg');
			//echo ' not actived image';
			echo file_get_contents('panel/static/img/sheet-not-actived.png');
			exit;
		}
		
		if(empty($file[file])) {
			header('Content-Type: image/jpeg');
			//echo 'row exits but not uploaded yet';
			echo file_get_contents('panel/static/img/sheet-not-uploaded.png');
		} else {
			
			
			if(in_array($_GET[file],array('first_jpg','second_jpg','task_jpg'))) {
				
				header('Content-Type: image/jpeg');
				
			} else {
				
				header('Content-Type: application/pdf');
				
				switch($_GET[file]){
					case 'first_pdf': 
						header('Content-Disposition: attachment; filename="آزمون اولیه جلسه '.$_GET[session].'.pdf"');
					break;
					case 'first_answers': 
						header('Content-Disposition: attachment; filename="پاسخنامه آزمون اولیه جلسه '.$_GET[session].'.pdf"');
					break;
					case 'second_pdf':
						header('Content-Disposition: attachment; filename="آزمون تکمیلی جلسه '.$_GET[session].'.pdf"');
					break;
					case 'second_answers': 
						header('Content-Disposition: attachment; filename="پاسخنامه آزمون تکمیلی جلسه '.$_GET[session].'.pdf"');
					break;
					case 'task_pdf': 
						header('Content-Disposition: attachment; filename="تکلیف جلسه '.$_GET[session].'.pdf"');	
					break;
					case 'task_answers':
						header('Content-Disposition: attachment; filename="پاسخنامه تکلیف جلسه '.$_GET[session].'.pdf"');
					break;
				}
			}
			
			echo $file[file];
		}

	}
	
	private function studentRegister() {

		require_once('core/library/message.class.php');
		require_once('core/library/validation.class.php');
		
		$message = new \message();
		
		if($this->getFirstRegister() != 0) 
			$message->fatal('ثبت نام اولیه قبلاً انجام شده ، لطفا صفحه را از اول بارگذاری کنید ');
		
		$v = new \validation(7,$MySQL,$message); //  فعلا از 11 تا شده 7 تا

		$v->post('firstname')->text(VLD_LEN_LASTNAME)->persianOnly();
		$v->post('lastname')->text(VLD_LEN_FIRSTNAME)->persianOnly();
		$v->post('birthday')->date();
		$v->post('tell')->pattern(REGEX_TELL);
		//$v->post('city_id')->enum(31);
		//$v->post('town_id')->enum(452);
		//$v->post('area_id')->enum(10);
		//$v->post('zone_id')->enum(10);
		$v->post('postal_code')->pattern(REGEX_POSTALCODE);
		$v->post('cell')->pattern(REGEX_CELL);
		$v->post('schools_id')->enum(20);


		if($v->check()){
			
			$p = $v->values();

			$result = $this->MySQL->query('UPDATE `students` SET 
				`firstname`="'.$p[firstname].'",
				`lastname`="'.$p[lastname].'",
				`birthday`="'.$p[birthday].'",
				`tell`="'.$p[tell].'",
				`city_id`="'.$p[city_id].'",
				`town_id`="'.$p[town_id].'",
				`area_id`="'.$p[area_id].'",
				`zone_id`="'.$p[zone_id].'",
				`postal_code`="'.$p[postal_code].'",
				`cell`="'.$p[cell].'",
				`schools_id`="'.$p[schools_id].'",
				`first_register`=1
				WHERE(students_id="'.$_SESSION[studentUserID].'");'
			);
			
			
			$this->MySQL->query('INSERT INTO `students_timeline` SET 
				`students_id`="'.$_SESSION[studentUserID].'",
				`timeline_event_id`=3,
				`date`=NOW();
			');

			if(!$result)
				$v->message->fatal('خطا از سمت دیتابیست :' . $this->MySQL->error);


			$v->message->add(XHHTP_SUCCESS_EDIT);
		}

		$v->message->showFirst();

	}
	
	private function studentChangePassword() {

		require_once('core/library/message.class.php');
		require_once('core/library/validation.class.php');
		
		$v = new \validation(3,$MySQL,$message);

		$v->post('password')->required();
		$v->post('new_password')->required();
		$v->post('repeat_password')->required();

		if($v->check()){
			
			$p = $v->values();
			
			$password = $this->MySQL->query('SELECT `password` FROM `students` WHERE(students_id="'.$_SESSION[studentUserID].'");')->fetch_assoc();

			if(!$password || $password[password] != md5($p[password])) {
				$v->message->fatal('رمز عبور وارد شده اشتباه است');
			}
			
			if($p[repeat_password] != $p[new_password]) {
				$v->message->fatal('رمز عبور جدید با تکرار آن برابر نیست');
			}

			$result = $this->MySQL->query('UPDATE `students` SET 
				`password`="'.md5($p[new_password]).'"
				WHERE(students_id="'.$_SESSION[studentUserID].'");'
			);
	
			$this->MySQL->query('INSERT INTO `students_timeline` SET 
				`students_id`="'.$_SESSION[studentUserID].'",
				`timeline_event_id`=4,
				`date`=NOW();
			');

			if(!$result)
				$v->message->fatal('خطا از سمت دیتابیست :' . $this->MySQL->error);
			
			$v->message->add(XHHTP_SUCCESS_EDIT);
		}

		$v->message->showFirst();
		
	}
	
	private function studentEditInfo() {

		require_once('core/library/message.class.php');
		require_once('core/library/validation.class.php');
		
		$v = new \validation(5,$MySQL,$message);

		$v->post('schools_id')->enum(50);
		$v->post('address')->text(VLD_LEN_FIRSTNAME)->persianOnly();
		$v->post('postal_code')->pattern(REGEX_POSTALCODE);
		$v->post('tell')->pattern(REGEX_TELL);
		$v->post('cell')->pattern(REGEX_CELL);

		if($v->check()){
			
			$p = $v->values();

			$result = $this->MySQL->query('UPDATE `students` SET 
				`schools_id`="'.$p[school_id].'",
				`address`="'.$p[address].'",
				`postal_code`="'.$p[postal_code].'",
				`tell`="'.$p[tell].'",
				`cell`="'.$p[cell].'"
				WHERE(students_id="'.$_SESSION[studentUserID].'");'
			);
			
			$this->MySQL->query('INSERT INTO `students_timeline` SET 
				`students_id`="'.$_SESSION[studentUserID].'",
				`timeline_event_id`=5,
				`date`=NOW();
			');

			if(!$result)
				$v->message->fatal('خطا از سمت دیتابیست :' . $this->MySQL->error);
			
			
			$v->message->add(XHHTP_SUCCESS_EDIT);

		}

		$v->message->showFirst();
	}

	private function studentAddPoints() {
		
		require_once('core/library/message.class.php');
		require_once('core/library/validation.class.php');
		
		$message = new \message();

		
		$v = new \validation(10,$MySQL,$message);
		
		$v->post('pack_id')->enum(1000);
		$v->post('session')->enum(10);
		$v->post('taskResult0')->enum(4);
		$v->post('taskResult1')->enum(4);
		$v->post('taskResult2')->enum(4);
		$v->post('taskResult3')->enum(4);
		$v->post('taskResult4')->enum(4);
		$v->post('taskResult5')->enum(4);
		$v->post('taskResult6')->enum(4);
		$v->post('comment')->text(600)->persianOnly();
		
		if($v->check()){
			
			$p = $v->values();
			
			/* check if this students are in this premium packs  and get premium_packs_students_id */
			$premium_packs_students = $this->MySQL->query('SELECT 
				`premium_packs_students_id`,
				`session'.$p[session].'_first`,
				`session'.$p[session].'_second`
				FROM `premium_packs_students`	
				WHERE(`premium_packs_id`="'.$p[pack_id].'" AND `students_id`="'.$_SESSION[studentUserID].'");'
			);

			/* check for security */
			if($premium_packs_students->num_rows != 1)
				$message->fatal('خطا : دریافت مقادیر نا معتبر از سمت اکانت کاربری شما');
			
			/* get ID  */
			list($premium_packs_students_id,$session_first,$session_second) = $premium_packs_students->fetch_array();
			
			/* check if task added before */
			$check_if_task_added = $this->MySQL->query('SELECT
				`premium_students_sessions_id` 
			FROM  `premium_students_sessions` 
			WHERE(
				`session`="'.$p[session].'" AND 
				`premium_packs_students_id`="'.$premium_packs_students_id.'" AND 
				`premium_packs_id`="'.$p[pack_id].'" AND 
				students_id="'.$_SESSION[studentUserID].'"
			);'
			)->num_rows;
			
			if($check_if_task_added != 0)
				$message->fatal('تکلیف این جلسه قبلا ٌثبت شده');


			$result = $this->MySQL->query('INSERT INTO `premium_students_sessions` SET 
				`premium_packs_id`="'.$p[pack_id].'",
				`premium_packs_students_id`="'.$premium_packs_students_id.'",
				`students_id`="'.$_SESSION[studentUserID].'",
				`session`="'.$p[session].'",
				`q1`="'.$p[taskResult0].'",
				`q2`="'.$p[taskResult1].'",
				`q3`="'.$p[taskResult2].'",
				`q4`="'.$p[taskResult3].'",
				`q5`="'.$p[taskResult4].'",
				`q6`="'.$p[taskResult5].'",
				`q7`="'.$p[taskResult6].'",
				`has_comment`="'.(!empty($p[comment])?1:0).'";'
			);
			
			if(!empty($p[comment])) {
				
				$this->MySQL->query('INSERT INTO `students_timeline` SET 
					`students_id`="'.$_SESSION[studentUserID].'",
					`timeline_event_id`=2,
					`date`=NOW();
				');
			
				
				$this->MySQL->query('INSERT INTO `premium_comments` SET 
					`premium_packs_students_id`="'.$premium_packs_students_id.'",
					`session`="'.$p[session].'",
					`text`="'.$p[comment].'",
					`date`=NOW(),
					`is_owner`=0;'
				);
			}
			
			$taskResult = 
				$this->calcPoints($p[taskResult0]) +
				$this->calcPoints($p[taskResult1]) +
				$this->calcPoints($p[taskResult2]) +
				$this->calcPoints($p[taskResult3]) +
				$this->calcPoints($p[taskResult4]) +
				$this->calcPoints($p[taskResult5]) +
				$this->calcPoints($p[taskResult6]);
			
			$result2 = $this->MySQL->query('UPDATE `premium_packs_students` SET 
				`session'.$p[session].'_task`="'.$taskResult.'" 
				WHERE(`premium_packs_students_id`="'.$premium_packs_students_id.'");'
			);
			
			/* امتیاز آزمون اول */
			$this->MySQL->query('INSERT INTO `students_reputation` SET
				`students_id`="'.$_SESSION[studentUserID].'",
				`reputation_type_id`=1,
				`reputation`="'.($session_first * 3).'";
			');
			
			/* امتیاز آزمون دوم */
			$this->MySQL->query('INSERT INTO `students_reputation` SET
				`students_id`="'.$_SESSION[studentUserID].'",
				`reputation_type_id`=2,
				`reputation`="'.($session_second * 2).'";
			');
			
			/* امتیاز تکلیف */
			$this->MySQL->query('INSERT INTO `students_reputation` SET
				`students_id`="'.$_SESSION[studentUserID].'",
				`reputation_type_id`=3,
				`reputation`="'.$taskResult.'";
			');
			
			/* give medal! ( medal_id = 4  مدال مشارکت ) */
			if($taskResult == 70) {
				
				/*   timeline  : id=7 Medal */
				$this->MySQL->query('INSERT INTO `students_timeline`
					(`students_id`,`timeline_event_id`,`var1`,`var2`,`date`)
					SELECT "'.$_SESSION[studentUserID].'",7,`title`,
						(SELECT `reputation` FROM `reputation_type` WHERE(`reputation_type_id` = 7)),
						NOW()
					FROM `medals` WHERE(`medals_id` = 4);
				');

				$this->MySQL->query('INSERT INTO 
					`students_medals` SET  
					`medals_id` = 4, 
					`students_id` = "'.$_SESSION[studentUserID].'", 
					`count` = 1,
					`date`=NOW() 
					ON DUPLICATE KEY UPDATE 
					`count` = `count`+1 , 
					`date`=NOW();'
				);
				
				/* give point  `reputation_type_id` = 7   مدال مشارکت  */
				$this->MySQL->query('INSERT INTO `students_reputation`
					(`students_id`,`reputation_type_id`,`reputation`)
					SELECT "'.$_SESSION[studentUserID].'",`reputation_type_id`,`reputation`
					FROM `reputation_type` WHERE(`reputation_type_id` = 7);'
				);
			}
			
			/* give medal! ( medal_id = 2   مدال توانایی ) */
			if($session_first == 70) {
				
				/*   timeline  : id=7 Medal */
				$this->MySQL->query('INSERT INTO `students_timeline`
					(`students_id`,`timeline_event_id`,`var1`,`var2`,`date`)
					SELECT "'.$_SESSION[studentUserID].'",7,`title`,
					(SELECT `reputation` FROM `reputation_type` WHERE(`reputation_type_id` = 5)),
					NOW()
					FROM `medals` WHERE(`medals_id` = 2);
				');

				$this->MySQL->query('INSERT INTO 
					`students_medals` SET  
					`medals_id` = 2, 
					`students_id` = "'.$_SESSION[studentUserID].'", 
					`count` = 1,
					`date`=NOW() 
					ON DUPLICATE KEY UPDATE 
					`count` = `count`+1 , 
					`date`=NOW();'
				);
				
				/* give point  `reputation_type_id` = 5  مدال توانایی  */
				$this->MySQL->query('INSERT INTO `students_reputation`
					(`students_id`,`reputation_type_id`,`reputation`)
					SELECT "'.$_SESSION[studentUserID].'",`reputation_type_id`,`reputation`
					FROM `reputation_type` WHERE(`reputation_type_id` = 5);'
				);
			}
			
			/* give medal! ( medal_id = 3   مدال گیرایی ) */
			if($session_second == 70) {
				
				/*   timeline  : id=7 Medal */
				$this->MySQL->query('INSERT INTO `students_timeline`
					(`students_id`,`timeline_event_id`,`var1`,`var2`,`date`)
					SELECT "'.$_SESSION[studentUserID].'",7,`title`,
					(SELECT `reputation` FROM `reputation_type` WHERE(`reputation_type_id` = 6)),
					NOW()
					FROM `medals` WHERE(`medals_id` = 3);
				');

				$this->MySQL->query('INSERT INTO 
					`students_medals` SET  
					`medals_id` = 3, 
					`students_id` = "'.$_SESSION[studentUserID].'", 
					`count` = 1,
					`date`=NOW() 
					ON DUPLICATE KEY UPDATE 
					`count` = `count`+1 , 
					`date`=NOW();'
				);
				
				/* give point  `reputation_type_id` = 6  مدال گیرایی  */
				$this->MySQL->query('INSERT INTO `students_reputation`
					(`students_id`,`reputation_type_id`,`reputation`)
					SELECT "'.$_SESSION[studentUserID].'",`reputation_type_id`,`reputation`
					FROM `reputation_type` WHERE(`reputation_type_id` = 6);'
				);
			}
			
			
						
			$this->MySQL->query('INSERT INTO `students_timeline` SET 
				`students_id`="'.$_SESSION[studentUserID].'",
				`timeline_event_id`=6,
				`var1`='.$p[session].',
				`var2`='.$taskResult.',
				`date`=NOW();
			');
			
			
			$this->updateReputation();
			
			if(!$result)
				$v->message->fatal('خطا از سمت دیتابیست :' . $this->MySQL->error);


			$v->message->add(XHHTP_SUCCESS_SAVED);
		}

		$v->message->showFirst();

	}
	
	private function calcPoints($option){
		switch($option) {
			case 1: return 0;
			case 2: return 4;
			case 3: return 8;
			case 4: return 10;
			default:
				return 0;
		}
	}

	private function addComment() {
		
		require_once('core/library/message.class.php');
		require_once('core/library/validation.class.php');
		
		$message = new \message();

		$v = new \validation(3,$MySQL,$message);
		
		$v->post('pack_id')->enum(1000);
		$v->post('session')->enum(10);
		$v->post('text')->text(255)->persianOnly();
		
		if($v->check()){
			
			$p = $v->values();
			
			/* check if this students are in this premium packs  and get premium_packs_students_id */
			$premium_packs_students = $this->MySQL->query('SELECT `premium_packs_students_id` FROM 
				`premium_packs_students`	
				WHERE(`premium_packs_id`="'.$p[pack_id].'" AND `students_id`="'.$_SESSION[studentUserID].'");'
			);

			/* check for security */
			if($premium_packs_students->num_rows != 1)
				$message->fatal('خطا : دریافت مقادیر نا معتبر از سمت اکانت کاربری شما');
			
			list($premium_packs_students_id) = $premium_packs_students->fetch_array();
			
			
			$result = $this->MySQL->query('INSERT INTO `premium_comments` SET 
				`premium_packs_students_id`="'.$premium_packs_students_id.'",
				`session`="'.$p[session].'",
				`text`="'.$p[text].'",
				`date`=NOW(),
				`is_owner`=0;'
			);
			
			$this->MySQL->query('INSERT INTO `students_timeline` SET 
				`students_id`="'.$_SESSION[studentUserID].'",
				`timeline_event_id`=1,
				`var1`="'.$p[session].'",
				`date`=NOW();
			');
				
			if(!$result)
				$v->message->fatal('خطا از سمت دیتابیست :' . $this->MySQL->error);
			
			$v->message->add(XHHTP_SUCCESS_SAVED);
		}

		$v->message->showFirst();
	}
};

?>
