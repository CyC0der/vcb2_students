﻿<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>پنل مدیریت دانش آموز</title>
    <link href="{static_path}/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{static_path}/lib/font-awesome/css/font-awesome.min.css">
    <!--if lt IE 9script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')-->
    <link rel="stylesheet" type="text/css" href="{static_path}/lib/jquery.icheck/skins/square/blue.css">
    <link rel="stylesheet" type="text/css" href="{static_path}/lib/jquery.gritter/jquery.gritter.css">
    <link href="{static_path}/css/style.css" rel="stylesheet">
    <style>
	
		.commentMode,.taskMode{
			display:none;
		}
		
        .table-addPoints th,
        .table-addPoints td {
            text-align: center;
        }
        
        .table-addPoints .primary-emphasis,
        .table-addPoints th {
            font-weight: bold;
        }		
		
		 .table-addPoints .form-group{
			margin:0;
		 }
        
        .student-session * {
            width: 65px;
        }
        
        .skills {
            margin: 0 auto 0 auto;
            width: 500px;
        }
        
        .skills a {
            float: left;
            width: 100px;
            text-align: center;
        }
        
        .task-comment .alert {
            padding: 10px;
            margin: 5px 0 0 0;
        }
        
        .task-comment textarea {
            margin-bottom: 10px;
        }
        
        .task-comment .well {
            padding: 10px;
        }
        /*Comment List styles*/
        
        .comment-list .row {
            margin-bottom: 0px;
        }
        
        .comment-list .panel .panel-heading {
            padding: 4px 15px;
            position: absolute;
            border: none;
            /*Panel-heading border radius*/
            border-top-right-radius: 0px;
            top: 1px;
        }
        
        .comment-list .panel .panel-heading.right {
            border-right-width: 0px;
            /*Panel-heading border radius*/
            border-top-left-radius: 0px;
            right: 16px;
        }
        
        .comment-list .panel .panel-heading .panel-body {
            padding-top: 6px;
        }
		
		
		.comment-list .panel-body{
			padding:10px;
		}
        
        .comment-list figcaption {
            /*For wrapping text in thumbnail*/
            word-wrap: break-word;
        }
        /* Portrait tablets and medium desktops */
        
        @media (min-width: 768px) {
            .comment-list .arrow:after,
            .comment-list .arrow:before {
                content: "";
                position: absolute;
                width: 0;
                height: 0;
                border-style: solid;
                border-color: transparent;
            }
            .comment-list .panel.arrow.left:after,
            .comment-list .panel.arrow.left:before {
                border-left: 0;
            }
            /*****Left Arrow*****/
            /*Outline effect style*/
            .comment-list .panel.arrow.left:before {
                left: 0px;
                top: 30px;
                /*Use boarder color of panel*/
                border-right-color: inherit;
                border-width: 16px;
            }
            /*Background color effect*/
            .comment-list .panel.arrow.left:after {
                left: 1px;
                top: 31px;
                /*Change for different outline color*/
                border-right-color: #FFFFFF;
                border-width: 15px;
            }
            /*****Right Arrow*****/
            /*Outline effect style*/
            .comment-list .panel.arrow.right:before {
                right: -16px;
                top: 30px;
                /*Use boarder color of panel*/
                border-left-color: inherit;
                border-width: 16px;
            }
            /*Background color effect*/
            .comment-list .panel.arrow.right:after {
                right: -14px;
                top: 31px;
                /*Change for different outline color*/
                border-left-color: #FFFFFF;
                border-width: 15px;
            }
        }
        
        .comment-list .comment-post {
            margin-top: 6px;
        }
		
		.comment-list .comment-post p{
            margin: 0px;
        }
		
		.comment-list .panel-default{
			margin:0;
		}
		
		.comment-list .row{
			padding:0;
			margin:0;
		}
        
        
        .profile-avatar {
            width: 150px;
        }
        
        .reputation {
            width: 120px;
            margin-left: 30px;
        }
        
        .reputation img {
            width: 30px;
            float: left;
        }
        
        .medals {
            width: 240px;
            margin-left: 30px;
        }
        
        .medals img {
            width: 50px;
            float: left;
        }
        
        #task-block {
            min-height: 700px;
        }
		
		
		.papers a{
			margin:5px 0 0 0;
		}
    </style>
<script type="text/x-tmpl" id="tmpl-comment-owner">
	<article class="row">
		<div class="col-md-2 col-sm-3 hidden-xs">
			<figure class="thumbnail">
				<img class="img-responsive" src="http://www.keita-gaming.com/assets/profile/default-avatar-c5d8ec086224cb6fc4e395f4ba3018c2.jpg" />
			</figure>
		</div>
		<div class="col-md-10 col-sm-9">
			<div class="panel panel-default arrow left">
				<div class="panel-body">
					<header class="text-left">
						<time class="comment-date" datetime="{%=o.date%}"><i class="fa fa-clock-o"></i> {%=o.date%}</time>
					</header>
					<div class="comment-post">
						<p>{%=o.text%}</p>
					</div>
				</div>
			</div>
		</div>
	</article>
</script>
<script type="text/x-tmpl" id="tmpl-comment-user">
	<article class="row">
		<div class="col-md-10 col-sm-9">
			<div class="panel panel-default arrow right">
				<div class="panel-body">
					<header class="text-right">
						<time class="comment-date" datetime="{%=o.date%}"><i class="fa fa-clock-o"></i> {%=o.date%}</time>
					</header>
					<div class="comment-post">
						<p>{%=o.text%}</p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-2 col-sm-3 hidden-xs">
			<figure class="thumbnail">
				<img class="img-responsive" src="{%=o.picture%}" />
			</figure>
		</div>
	</article>
</script>
<script type="text/x-tmpl" id="tmpl-timeline">
	<li><i class="fa fa-{%=o.icon%}"></i>
	<span class="date">{%=o.date%}</span>
		<div class="content">
			<strong>{%=o.title%}</strong>
			<p>{%#o.text%}</p>
			<small>{%=o.time%}</small>
		</div>
	</li>
</script>
<script type="text/x-tmpl" id="tmpl-reputation">

{% for (var i=0; i<o.rep.length; i++) { %}
   <img src="{static_path}/img/point/{%=o.rep[i]%}.png" />
{% } %}
<h4>امتیاز</h4>
</script>
<script type="text/x-tmpl" id="tmpl-medals">
{% for (var i=0; i<o.medals.length; i++) { %}
<div style="poition:relative;width:80px;height:60px;float:right">
	<h4 style="float:left">{%=o.medals[i][1]%}</h4>
   <img src="{static_path}/img/medals/{%=o.medals[i][0]%}.png" />
</div>
{% } %}
</script>
</head>

<body>
    <!-- Navbar - Start -->
    <div id="head-nav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header navbar-left">
                <button type="button" data-toggle="collapse" data-target=".navbar-collapse" class="navbar-toggle">
                    <span class="fa fa-gear"></span>
                </button>
                <a href="#" class="navbar-brand"><span>آموزش ریاضیات مهندس محمد خوانساری</span></a>
            </div>
            <div class="navbar-collapse collapse">

                <ul class="nav navbar-nav navbar-right user-nav">
                    <li class="dropdown profile_menu">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                            <b class="caret"></b>
                            <span>{student_name}</span>
                            <img alt="Avatar" src="{static_path}/img/avatar2.jpg">
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="?r=index/logout">خروج</a></li>
                        </ul>
                    </li>
                </ul>


            </div>
        </div>
    </div>
    <!-- Navbar - End -->

    <!-- ourBody START -->
    <div id="cl-wrapper">

        <div id="pcont" class="container-fluid">
            <!--NedaArea START-->
            <div class="cl-mcont">

                <div class="row">
                    <div class="col-sm-5">
                        <div class="tab-container">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tabInfo">اطللاعات</a></li>
                                <li><a data-toggle="tab" href="#tabEditResellerModirator">تغییر مشخصات</a></li>
                                <li><a data-toggle="tab" href="#tabStudentChangePassword">تغییر رمزعبور</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="tabInfo" class="tab-pane active cont">

                                    <div class="avatar-upload pull-left">
                                        <img src="{static_path}/img/student-profile.png" class=" profile-avatar img-thumbnail">
                                        <!--<input id="fileupload" type="file" name="files[]">-->
                                        <div id="progress" class="overlay"></div>
                                    </div>
                                    <div class="reputation pull-left">
                                    </div>
                                    <div class="medals pull-left">
                                        <img src="{static_path}/img/medals/3.png" />
                                        <img src="{static_path}/img/medals/2.png" />
                                        <img src="{static_path}/img/medals/4.png" />
                                    </div>

                                    <div class="clearfix"></div>
                                </div>
                                <div id="tabEditResellerModirator" class="tab-pane">
                                    <form role="form" class="form-tooltip" name="studentEditInfo">
                                        <div class="form-group">
                                            <label for="schools_id">نام مدرسه :</label>
                                            <select class="form-control input-sm" name="schools_id" required>
                                                <option value=""> ---</option>
                                                <option value="2"> دانا</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="address"> آدرس محل سکونت :</label>
                                            <input type="text" class="form-control input-sm" name="address" required title="ب فارسی تایپ کنید.">
                                        </div>
                                        <div class="form-group">
                                            <label for="postal_code"> کدپستی:</label>
                                            <input type="text" class="form-control input-sm" name="postal_code" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="tell"> تلفن ثابت :</label>
                                            <input type="text" class="form-control input-sm" name="tell" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="cell"> شماره همراه :</label>
                                            <input type="text" class="form-control input-sm" name="cell" required>
                                        </div>
                                        <button type="submit" class="btn btn-primary">ارسال</button>
                                    </form>
                                </div>
                                <div id="tabStudentChangePassword" class="tab-pane">
                                    <form role="form" name="studentChangePassword">
                                        <div class="form-group">
                                            <label for="password"> رمز عبور فعلی :</label>
                                            <input type="password" class="form-control" name="password" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="new_password"> رمز عبور جدید :</label>
                                            <input class="form-control" type="password" name="new_password" id="pass" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="repeat_password"> رمز عبور جدید :</label>
                                            <input class="form-control" type="password" name="repeat_password" data-parsley-equalto="#pass" required>
                                        </div>
                                        <button type="submit" class="btn btn-primary">تغییر رمزعبور</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <ul class="timeline">
                        </ul>
                    </div>
                    <div class="col-sm-7 side-left">

                        <div class="block-flat">
                            <div class="header">
                                <h4><i class="glyphicon glyphicon-stats"></i> نمودار نمرات آزمون ها </h4>
                            </div>
                            <div class="content">
                                <img id="graph_img" style="width:100%" src="?r=graph" />
                            </div>

                        </div>

                        <div id="task-block" class="block-flat">
                            <div class="header">
                                <h4><i class="glyphicon glyphicon-stats"></i> تکالیف </h4>
                               <!-- <span class="badge badge-danger" style="margin-bottom:10px">دو تکلیف انجام نشده</span>-->
                            </div>
                            <div class="content">
                                <b>جلسه:</b>
                                <div class="btn-group student-session" role="group" style="margin-bottom:10px;">
                                    <button type="button" class="btn btn-default disabled">دهم</button>
                                    <button type="button" class="btn btn-default disabled">نهم</button>
                                    <button type="button" class="btn btn-default disabled">هشتم</button>
                                    <button type="button" class="btn btn-default disabled">هفتم</button>
                                    <button type="button" class="btn btn-default disabled">ششم</button>
                                    <button type="button" class="btn btn-default disabled">پنجم</button>
                                    <button type="button" class="btn btn-default disabled">چهارم</button>
                                    <button type="button" class="btn btn-default disabled">سوم</button>
                                    <button type="button" class="btn btn-default disabled">دوم</button>
                                    <button type="button" class="btn btn-default disabled">اول</button>
                                </div>

                                <div class="session-box">
	
									<div class="commentMode">
										<div class="alert alert-success alert-white rounded">
											<button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
											<div class="icon"><i class="fa fa-check"></i></div>تکلیف این جلسه ثبت شد و نتیجه آن در نمودار قابل مشاهده است
										</div>

										<div class="papers row">
											<div class="col-sm-6 col-md-4">
												<div class="thumbnail">
													<div class="caption text-center">
														<span class="label label-primary">آزمون اولیه</span>
													</div>
													<img id="first_jpg"  src="index.php?r=index/download&pack_id=1&session=1&file=first_jpg" alt="آزمون اولیه">
													<a href="index.php?r=index/download&pack_id=1&session=1&file=first_pdf" id="first_pdf" class="btn btn-xs btn-primary">دانلود آزمون</a>
													<a href="index.php?r=index/download&pack_id=1&session=1&file=first_answers" id="first_answers" class="btn btn-xs btn-primary">دانلود پاسخنامه</a>
												</div>
											</div>
											<div class="col-sm-6 col-md-4">
												<div class="thumbnail">
													<div class="caption text-center">
														<span class="label label-success">آزمون تکمیلی</span>
													</div>
													<img id="second_jpg" src="index.php?r=index/download&pack_id=1&session=1&file=second_jpg" alt="آزمون تکمیلی">
													<a href="index.php?r=index/download&pack_id=1&session=1&file=second_pdf" id="second_pdf" class="btn btn-xs btn-primary">دانلود آزمون</a>
													<a href="index.php?r=index/download&pack_id=1&session=1&file=second_answers" id="second_answers" class="btn btn-xs btn-primary">دانلود پاسخنامه</a>
												</div>
											</div>
											<div class="col-sm-6 col-md-4">
												<div class="thumbnail">
													<div class="caption text-center">
														<span class="label label-danger">برگه تکلیف</span>
													</div>
													<img id="task_jpg"  src="index.php?r=index/download&pack_id=1&session=1&file=task_jpg" alt="برگه تکلیف">
													<a href="index.php?r=index/download&pack_id=1&session=1&file=task_pdf" id="task_pdf" class="btn btn-xs btn-primary">دانلود آزمون</a>
													<a href="index.php?r=index/download&pack_id=1&session=1&file=task_answers" id="task_answers" class="btn btn-xs btn-primary">دانلود پاسخنامه</a>
												</div>
											</div>
										</div>

										<section class="task-comment comment-list">
											<form name="addComment">
											<div class="well">
												دیدگاه خود را بنویسید:
												<div class="form-group">
													<textarea required name="text" placeholder="پیام اولیا" class="form-control"></textarea>
												</div>
												<button type="submit" class="btn btn-default btn-xs">ارسال دیدگاه</button>
											</div>
											</form>
										</section>
									</div>
									
                                    <div class="taskMode">
                                        <div class="alert alert-info alert-white rounded">
                                            <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
                                            <div class="icon"><i class="fa fa-info-circle"></i></div>درج تکایف و بهمان و بیستان
                                        </div>
                                        <form name="studentAddPoints">
                                            <table class="table-addPoints no-border">

                                                <thead class="no-border">
                                                    <tr>
                                                        <th class="primary-emphasis" style="width:20%">سوال</th>
                                                        <th style="width:20%">هیچ</th>
                                                        <th style="width:20%">ضعیف</th>
                                                        <th style="width:20%">نیمه کامل</th>
                                                        <th style="width:20%">کامل</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="no-border-x no-border-y">
                                                    <tr>
                                                        <td class="primary-emphasis">1 -</td>
                                                        <td>
                                                            <div class="form-group">
																<input type="radio" name="taskResult0" value="1" class="icheck" required>
															</div>
                                                        </td>
                                                        <td>
															<div class="form-group">
																<input type="radio" name="taskResult0" value="2" class="icheck" required>
															</div>
                                                        </td>
                                                        <td>
															<div class="form-group">
																<input type="radio" name="taskResult0" value="3" class="icheck" required>
															</div>
                                                        </td>
                                                        <td>
															<div class="form-group">
																<input type="radio" name="taskResult0" value="4" class="icheck" required>
															</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="primary-emphasis">2 -</td>
                                                        <td>
                                                            <div class="form-group">
																<input type="radio" name="taskResult1" value="1" class="icheck" required>
															</div>
                                                        </td>
                                                        <td>
															<div class="form-group">
																<input type="radio" name="taskResult1" value="2" class="icheck" required>
															</div>
                                                        </td>
                                                        <td>
															<div class="form-group">
																<input type="radio" name="taskResult1" value="3" class="icheck" required>
															</div>
                                                        </td>
                                                        <td>
															<div class="form-group">
																<input type="radio" name="taskResult1" value="4" class="icheck" required>
															</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="primary-emphasis">3 -</td>
                                                         <td>
                                                            <div class="form-group">
																<input type="radio" name="taskResult2" value="1" class="icheck" required>
															</div>
                                                        </td>
                                                        <td>
															<div class="form-group">
																<input type="radio" name="taskResult2" value="2" class="icheck" required>
															</div>
                                                        </td>
                                                        <td>
															<div class="form-group">
																<input type="radio" name="taskResult2" value="3" class="icheck" required>
															</div>
                                                        </td>
                                                        <td>
															<div class="form-group">
																<input type="radio" name="taskResult2" value="4" class="icheck" required>
															</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="primary-emphasis">4 -</td>
                                                        <td>
                                                            <div class="form-group">
																<input type="radio" name="taskResult3" value="1" class="icheck" required>
															</div>
                                                        </td>
                                                        <td>
															<div class="form-group">
																<input type="radio" name="taskResult3" value="2" class="icheck" required>
															</div>
                                                        </td>
                                                        <td>
															<div class="form-group">
																<input type="radio" name="taskResult3" value="3" class="icheck" required>
															</div>
                                                        </td>
                                                        <td>
															<div class="form-group">
																<input type="radio" name="taskResult3" value="4" class="icheck" required>
															</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="primary-emphasis">5 -</td>
                                                        <td>
                                                            <div class="form-group">
																<input type="radio" name="taskResult4" value="1" class="icheck" required>
															</div>
                                                        </td>
                                                        <td>
															<div class="form-group">
																<input type="radio" name="taskResult4" value="2" class="icheck" required>
															</div>
                                                        </td>
                                                        <td>
															<div class="form-group">
																<input type="radio" name="taskResult4" value="3" class="icheck" required>
															</div>
                                                        </td>
                                                        <td>
															<div class="form-group">
																<input type="radio" name="taskResult4" value="4" class="icheck" required>
															</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="primary-emphasis">6 -</td>
                                                        <td>
                                                            <div class="form-group">
																<input type="radio" name="taskResult5" value="1" class="icheck" required>
															</div>
                                                        </td>
                                                        <td>
															<div class="form-group">
																<input type="radio" name="taskResult5" value="2" class="icheck" required>
															</div>
                                                        </td>
                                                        <td>
															<div class="form-group">
																<input type="radio" name="taskResult5" value="3" class="icheck" required>
															</div>
                                                        </td>
                                                        <td>
															<div class="form-group">
																<input type="radio" name="taskResult5" value="4" class="icheck" required>
															</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="primary-emphasis">7 -</td>
                                                        <td>
                                                            <div class="form-group">
																<input type="radio" name="taskResult6" value="1" class="icheck" required>
															</div>
                                                        </td>
                                                        <td>
															<div class="form-group">
																<input type="radio" name="taskResult6" value="2" class="icheck" required>
															</div>
                                                        </td>
                                                        <td>
															<div class="form-group">
																<input type="radio" name="taskResult6" value="3" class="icheck" required>
															</div>
                                                        </td>
                                                        <td>
															<div class="form-group">
																<input type="radio" name="taskResult6" value="4" class="icheck" required>
															</div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                                <tfoot>
                                                    <td colspan="5">
														
                                                        <div class="well">
															<div class="form-group">
																<textarea name="comment" placeholder="پیام اولیا" class="form-control" required></textarea>
															</div>
                                                        </div>
														
                                                        <button type="submit" class="btn btn-success">ثبت تکلیف</button>
                                                    </td>
                                                </tfoot>
                                            </table>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--NedaArea END -->
        </div>
    </div>

    <!--modal-->
    <div class="modal fade" id="win-studentRegister" tabindex="-1 " role="dialog">
        <div class="modal-dialog" role="document">
            <div class="block-flat">
                <div class="header">
                    <h4>فرم ثبت نام اولیه</h4>
                </div>
                <div class="content">
                    <form role="form" class="form-tooltip" name="studentRegister">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tell"> شماره تماس ثابت :</label>
                                    <input type="text" required class="form-control input-sm" name="tell" title="قبلا توسط مدرس تکمیل شده است. در صورتی که اشتباه است آن را درست کنید" placeholder="37272110">
                                </div>
                                <div class="form-group">
                                    <label for="postal_code"> کد پستی :</label>
                                    <input type="text" required class="form-control input-sm" name="postal_code" title="10 رقم ،بدون خط فاصله" placeholder="4236987541">
                                </div>
                                <div class="form-group">
                                    <label for="cell"> شماره تماس اولیا :</label>
                                    <input type="text" required class="form-control input-sm" name="cell" placeholder="09">
                                </div>
                                <div class="form-group">
                                    <label for="schools_id"> نام مدرسه :</label>
                                    <select required class="form-control input-sm" name="schools_id" title="قبلا توسط مدرس تکمیل شده است. در صورتی که اشتباه است آن را درست کنید.">
                                        <option value=""> ---</option>
                                        <option value="1">دانا</option>
                                    </select>
                                </div>

                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstname">نام :</label>
                                    <input type="text" required class="form-control input-sm" name="firstname" title="قبلا توسط مدرس تکمیل شده است. در صورتی که اشتباه است آن را درست کنید">
                                </div>
                                <div class="form-group">
                                    <label for="lastname"> نام خانوادگی :</label>
                                    <input type="text" required class="form-control input-sm" name="lastname" title="به فارسی تایپ کنید.">
                                </div>
                                <h5> تاریخ تولد : </h5>
                                <div class="row form-inline-custom">
                                    <div class="form-group col-md-4">
                                        <label for="birthday_year" class="sr-only">سال</label>
                                        <select class="form-control input-sm" required name="birthday_year">
                                            <option value="">- سال -</option>
                                            <option value="1350">1350</option>
                                            <option value="1351">1351</option>
                                            <option value="1352">1352</option>
                                            <option value="1353">1353</option>
                                            <option value="1354">1354</option>
                                            <option value="1355">1355</option>
                                            <option value="1356">1356</option>
                                            <option value="1357">1357</option>
                                            <option value="1358">1358</option>
                                            <option value="1359">1359</option>
                                            <option value="1360">1360</option>
                                            <option value="1361">1361</option>
                                            <option value="1362">1362</option>
                                            <option value="1363">1363</option>
                                            <option value="1364">1364</option>
                                            <option value="1365">1365</option>
                                            <option value="1366">1366</option>
                                            <option value="1367">1367</option>
                                            <option value="1368">1368</option>
                                            <option value="1369">1369</option>
                                            <option value="1370">1370</option>
                                            <option value="1371">1371</option>
                                            <option value="1372">1372</option>
                                            <option value="1373">1373</option>
                                            <option value="1374">1374</option>
                                            <option value="1375">1375</option>
                                            <option value="1376">1376</option>
                                            <option value="1377">1377</option>
                                            <option value="1378">1378</option>
                                            <option value="1379">1379</option>
                                            <option value="1380">1380</option>
											<option value="1380">1381</option>
											<option value="1380">1382</option>
											<option value="1380">1383</option>
											<option value="1380">1384</option>
											<option value="1380">1385</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="birthday_month" class="sr-only">ماه</label>
                                        <select required class="form-control input-sm" name="birthday_month">
                                            <option value="">- ماه -</option>
                                            <option value="1">فروردین-ماه اول</option>
                                            <option value="2">اردیبهشت-ماه دوم</option>
                                            <option value="3">خرداد-ماه سوم</option>
                                            <option value="4">تیر-ماه چهارم</option>
                                            <option value="5">مرداد-ماه پنجم</option>
                                            <option value="6">شهریور-ماه ششم</option>
                                            <option value="7">مهر-ماه هفتم</option>
                                            <option value="8">آبان-ماه هشتم</option>
                                            <option value="9">آذر-ماه نهم </option>
                                            <option value="10">دی-ماه دهم</option>
                                            <option value="11">بهمن-ماه یازدهم</option>
                                            <option value="12">اسفند-ماه دوازدهم</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="birthday_day" class="sr-only"> روز </label>
                                        <select required class="form-control input-sm" name="birthday_day">
                                            <option value="">- روز -</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                            <option value="24">24</option>
                                            <option value="25">25</option>
                                            <option value="26">26</option>
                                            <option value="27">27</option>
                                            <option value="28">28</option>
                                            <option value="29">29</option>
                                            <option value="30">30</option>
                                            <option value="31">31</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- <div class="row form-inline-custom">
                                    <div class="form-group col-md-6">
                                        <label for="town_id" class="sr-only"> شهرستان :</label>
                                        <select class="form-control input-sm disabled" required disabled name="town_id">
                                            <option value="" data-city-id="">- شهرستان -</option>
                                            <option value="1" data-city-id="1">آذر شهر</option>
                                            <option value="2" data-city-id="1">اسكو</option>
                                            <option value="3" data-city-id="1">اهر</option>
                                            <option value="4" data-city-id="1">بستان آباد</option>
                                            <option value="5" data-city-id="1">بناب</option>
                                            <option value="6" data-city-id="1">بندر شرفخانه</option>
                                            <option value="7" data-city-id="1">تبريز</option>
                                            <option value="8" data-city-id="1">تسوج</option>
                                            <option value="9" data-city-id="1">جلفا</option>
                                            <option value="10" data-city-id="1">سراب</option>
                                            <option value="11" data-city-id="1">شبستر</option>
                                            <option value="12" data-city-id="1">صوفیان</option>
                                            <option value="13" data-city-id="1">عجبشير</option>
                                            <option value="14" data-city-id="1">قره آغاج</option>
                                            <option value="15" data-city-id="1">كليبر</option>
                                            <option value="16" data-city-id="1">كندوان</option>
                                            <option value="17" data-city-id="1">مراغه</option>
                                            <option value="18" data-city-id="1">مرند</option>
                                            <option value="19" data-city-id="1">ملكان</option>
                                            <option value="20" data-city-id="1">ممقان</option>
                                            <option value="21" data-city-id="1">ميانه</option>
                                            <option value="22" data-city-id="1">هاديشهر</option>
                                            <option value="23" data-city-id="1">هريس</option>
                                            <option value="24" data-city-id="1">هشترود</option>
                                            <option value="25" data-city-id="1">ورزقان</option>
                                            <option value="26" data-city-id="2">اروميه</option>
                                            <option value="27" data-city-id="2">اشنويه</option>
                                            <option value="28" data-city-id="2">بازرگان</option>
                                            <option value="29" data-city-id="2">بوكان</option>
                                            <option value="30" data-city-id="2">پلدشت</option>
                                            <option value="31" data-city-id="2">پيرانشهر</option>
                                            <option value="32" data-city-id="2">تكاب</option>
                                            <option value="33" data-city-id="2">خوي</option>
                                            <option value="34" data-city-id="2">سردشت</option>
                                            <option value="35" data-city-id="2">سلماس</option>
                                            <option value="36" data-city-id="2">سيه چشمه- چالدران</option>
                                            <option value="37" data-city-id="2">سیمینه</option>
                                            <option value="38" data-city-id="2">شاهين دژ</option>
                                            <option value="39" data-city-id="2">شوط</option>
                                            <option value="40" data-city-id="2">ماكو</option>
                                            <option value="41" data-city-id="2">مهاباد</option>
                                            <option value="42" data-city-id="2">مياندوآب</option>
                                            <option value="43" data-city-id="2">نقده</option>
                                            <option value="44" data-city-id="3">اردبيل</option>
                                            <option value="45" data-city-id="3">بيله سوار</option>
                                            <option value="46" data-city-id="3">پارس آباد</option>
                                            <option value="47" data-city-id="3">خلخال</option>
                                            <option value="48" data-city-id="3">سرعين</option>
                                            <option value="49" data-city-id="3">كيوي (كوثر)</option>
                                            <option value="50" data-city-id="3">گرمي (مغان)</option>
                                            <option value="51" data-city-id="3">مشگين شهر</option>
                                            <option value="52" data-city-id="3">مغان (سمنان)</option>
                                            <option value="53" data-city-id="3">نمين</option>
                                            <option value="54" data-city-id="3">نير</option>
                                            <option value="55" data-city-id="4">آران و بيدگل</option>
                                            <option value="56" data-city-id="4">اردستان</option>
                                            <option value="57" data-city-id="4">اصفهان</option>
                                            <option value="58" data-city-id="4">باغ بهادران</option>
                                            <option value="59" data-city-id="4">تيران</option>
                                            <option value="60" data-city-id="4">خميني شهر</option>
                                            <option value="61" data-city-id="4">خوانسار</option>
                                            <option value="62" data-city-id="4">دهاقان</option>
                                            <option value="63" data-city-id="4">دولت آباد-اصفهان</option>
                                            <option value="64" data-city-id="4">زرين شهر</option>
                                            <option value="65" data-city-id="4">زيباشهر (محمديه)</option>
                                            <option value="66" data-city-id="4">سميرم</option>
                                            <option value="67" data-city-id="4">شاهين شهر</option>
                                            <option value="68" data-city-id="4">شهرضا</option>
                                            <option value="69" data-city-id="4">فريدن</option>
                                            <option value="70" data-city-id="4">فريدون شهر</option>
                                            <option value="71" data-city-id="4">فلاورجان</option>
                                            <option value="72" data-city-id="4">فولاد شهر</option>
                                            <option value="73" data-city-id="4">قهدریجان</option>
                                            <option value="74" data-city-id="4">كاشان</option>
                                            <option value="75" data-city-id="4">گلپايگان</option>
                                            <option value="76" data-city-id="4">گلدشت اصفهان</option>
                                            <option value="77" data-city-id="4">گلدشت مركزی</option>
                                            <option value="78" data-city-id="4">مباركه اصفهان</option>
                                            <option value="79" data-city-id="4">مهاباد-اصفهان</option>
                                            <option value="80" data-city-id="4">نايين</option>
                                            <option value="81" data-city-id="4">نجف آباد</option>
                                            <option value="82" data-city-id="4">نطنز</option>
                                            <option value="83" data-city-id="4">هرند</option>
                                            <option value="84" data-city-id="5">آسارا</option>
                                            <option value="85" data-city-id="5">اشتهارد</option>
                                            <option value="86" data-city-id="5">شهر جديد هشتگرد</option>
                                            <option value="87" data-city-id="5">طالقان</option>
                                            <option value="88" data-city-id="5">كرج</option>
                                            <option value="89" data-city-id="5">گلستان تهران</option>
                                            <option value="90" data-city-id="5">نظرآباد</option>
                                            <option value="91" data-city-id="5">هشتگرد</option>
                                            <option value="92" data-city-id="6">آبدانان</option>
                                            <option value="93" data-city-id="6">ايلام</option>
                                            <option value="94" data-city-id="6">ايوان</option>
                                            <option value="95" data-city-id="6">دره شهر</option>
                                            <option value="96" data-city-id="6">دهلران</option>
                                            <option value="97" data-city-id="6">سرابله</option>
                                            <option value="98" data-city-id="6">شيروان چرداول</option>
                                            <option value="99" data-city-id="6">مهران</option>
                                            <option value="100" data-city-id="7">آبپخش</option>
                                            <option value="101" data-city-id="7">اهرم</option>
                                            <option value="102" data-city-id="7">برازجان</option>
                                            <option value="103" data-city-id="7">بندر دير</option>
                                            <option value="104" data-city-id="7">بندر ديلم</option>
                                            <option value="105" data-city-id="7">بندر كنگان</option>
                                            <option value="106" data-city-id="7">بندر گناوه</option>
                                            <option value="107" data-city-id="7">بوشهر</option>
                                            <option value="108" data-city-id="7">تنگستان</option>
                                            <option value="109" data-city-id="7">جزيره خارك</option>
                                            <option value="110" data-city-id="7">جم (ولايت)</option>
                                            <option value="111" data-city-id="7">خورموج</option>
                                            <option value="112" data-city-id="7">دشتستان - شبانکاره</option>
                                            <option value="113" data-city-id="7">دلوار</option>
                                            <option value="114" data-city-id="7">عسلویه</option>
                                            <option value="115" data-city-id="8">اسلامشهر</option>
                                            <option value="116" data-city-id="8">بومهن</option>
                                            <option value="117" data-city-id="8">پاكدشت</option>
                                            <option value="118" data-city-id="8">تهران</option>
                                            <option value="119" data-city-id="8">چهاردانگه</option>
                                            <option value="120" data-city-id="8">دماوند</option>
                                            <option value="121" data-city-id="8">رودهن</option>
                                            <option value="122" data-city-id="8">ري</option>
                                            <option value="123" data-city-id="8">شريف آباد</option>
                                            <option value="124" data-city-id="8">شهر رباط كريم</option>
                                            <option value="125" data-city-id="8">شهر شهريار</option>
                                            <option value="126" data-city-id="8">فشم</option>
                                            <option value="127" data-city-id="8">فيروزكوه</option>
                                            <option value="128" data-city-id="8">قدس</option>
                                            <option value="129" data-city-id="8">كهريزك</option>
                                            <option value="130" data-city-id="8">لواسان بزرگ</option>
                                            <option value="131" data-city-id="8">ملارد</option>
                                            <option value="132" data-city-id="8">ورامين</option>
                                            <option value="133" data-city-id="9">اردل</option>
                                            <option value="134" data-city-id="9">بروجن</option>
                                            <option value="135" data-city-id="9">چلگرد (كوهرنگ)</option>
                                            <option value="136" data-city-id="9">سامان</option>
                                            <option value="137" data-city-id="9">شهركرد</option>
                                            <option value="138" data-city-id="9">فارسان</option>
                                            <option value="139" data-city-id="9">لردگان</option>
                                            <option value="140" data-city-id="10">بشرویه</option>
                                            <option value="141" data-city-id="10">بيرجند</option>
                                            <option value="142" data-city-id="10">خضری</option>
                                            <option value="143" data-city-id="10">خوسف</option>
                                            <option value="144" data-city-id="10">سرایان</option>
                                            <option value="145" data-city-id="10">سربيشه</option>
                                            <option value="146" data-city-id="10">طبس</option>
                                            <option value="147" data-city-id="10">فردوس</option>
                                            <option value="148" data-city-id="10">قائن</option>
                                            <option value="149" data-city-id="10">نهبندان</option>
                                            <option value="150" data-city-id="11">بجستان</option>
                                            <option value="151" data-city-id="11">بردسكن</option>
                                            <option value="152" data-city-id="11">تايباد</option>
                                            <option value="153" data-city-id="11">تربت جام</option>
                                            <option value="154" data-city-id="11">تربت حيدريه</option>
                                            <option value="155" data-city-id="11">جغتای</option>
                                            <option value="156" data-city-id="11">جوین</option>
                                            <option value="157" data-city-id="11">چناران</option>
                                            <option value="158" data-city-id="11">خلیل آباد</option>
                                            <option value="159" data-city-id="11">خواف</option>
                                            <option value="160" data-city-id="11">درگز</option>
                                            <option value="161" data-city-id="11">رشتخوار</option>
                                            <option value="162" data-city-id="11">سبزوار</option>
                                            <option value="163" data-city-id="11">سرخس</option>
                                            <option value="164" data-city-id="11">طبس</option>
                                            <option value="165" data-city-id="11">طرقبه</option>
                                            <option value="166" data-city-id="11">فريمان</option>
                                            <option value="167" data-city-id="11">قوچان</option>
                                            <option value="168" data-city-id="11">كاشمر</option>
                                            <option value="169" data-city-id="11">كلات</option>
                                            <option value="170" data-city-id="11">گناباد</option>
                                            <option value="171" data-city-id="11">مشهد</option>
                                            <option value="172" data-city-id="11">نيشابور</option>
                                            <option value="173" data-city-id="12">آشخانه، مانه و سمرقان</option>
                                            <option value="174" data-city-id="12">اسفراين</option>
                                            <option value="175" data-city-id="12">بجنورد</option>
                                            <option value="176" data-city-id="12">جاجرم</option>
                                            <option value="177" data-city-id="12">شيروان</option>
                                            <option value="178" data-city-id="12">فاروج</option>
                                            <option value="179" data-city-id="13">آبادان</option>
                                            <option value="180" data-city-id="13">اميديه</option>
                                            <option value="181" data-city-id="13">انديمشك</option>
                                            <option value="182" data-city-id="13">اهواز</option>
                                            <option value="183" data-city-id="13">ايذه</option>
                                            <option value="184" data-city-id="13">باغ ملك</option>
                                            <option value="185" data-city-id="13">بستان</option>
                                            <option value="186" data-city-id="13">بندر ماهشهر</option>
                                            <option value="187" data-city-id="13">بندرامام خميني</option>
                                            <option value="188" data-city-id="13">بهبهان</option>
                                            <option value="189" data-city-id="13">خرمشهر</option>
                                            <option value="190" data-city-id="13">دزفول</option>
                                            <option value="191" data-city-id="13">رامشیر</option>
                                            <option value="192" data-city-id="13">رامهرمز</option>
                                            <option value="193" data-city-id="13">سوسنگرد</option>
                                            <option value="194" data-city-id="13">شادگان</option>
                                            <option value="195" data-city-id="13">شوش</option>
                                            <option value="196" data-city-id="13">شوشتر</option>
                                            <option value="197" data-city-id="13">لالي</option>
                                            <option value="198" data-city-id="13">مسجد سليمان</option>
                                            <option value="199" data-city-id="13">هنديجان</option>
                                            <option value="200" data-city-id="13">هويزه</option>
                                            <option value="201" data-city-id="14">آب بر (طارم)</option>
                                            <option value="202" data-city-id="14">ابهر</option>
                                            <option value="203" data-city-id="14">خرمدره</option>
                                            <option value="204" data-city-id="14">زرین آباد (ایجرود)</option>
                                            <option value="205" data-city-id="14">زنجان</option>
                                            <option value="206" data-city-id="14">قیدار (خدا بنده)</option>
                                            <option value="207" data-city-id="14">ماهنشان</option>
                                            <option value="208" data-city-id="15">ايوانكي</option>
                                            <option value="209" data-city-id="15">بسطام</option>
                                            <option value="210" data-city-id="15">دامغان</option>
                                            <option value="211" data-city-id="15">سرخه</option>
                                            <option value="212" data-city-id="15">سمنان</option>
                                            <option value="213" data-city-id="15">شاهرود</option>
                                            <option value="214" data-city-id="15">شهمیرزاد</option>
                                            <option value="215" data-city-id="15">گرمسار</option>
                                            <option value="216" data-city-id="15">مهدیشهر</option>
                                            <option value="217" data-city-id="16">ايرانشهر</option>
                                            <option value="218" data-city-id="16">چابهار</option>
                                            <option value="219" data-city-id="16">خاش</option>
                                            <option value="220" data-city-id="16">راسك</option>
                                            <option value="221" data-city-id="16">زابل</option>
                                            <option value="222" data-city-id="16">زاهدان</option>
                                            <option value="223" data-city-id="16">سراوان</option>
                                            <option value="224" data-city-id="16">سرباز</option>
                                            <option value="225" data-city-id="16">ميرجاوه</option>
                                            <option value="226" data-city-id="16">نيكشهر</option>
                                            <option value="227" data-city-id="17">آباده</option>
                                            <option value="228" data-city-id="17">آباده طشك</option>
                                            <option value="229" data-city-id="17">اردكان</option>
                                            <option value="230" data-city-id="17">ارسنجان</option>
                                            <option value="231" data-city-id="17">استهبان</option>
                                            <option value="232" data-city-id="17">اشكنان</option>
                                            <option value="233" data-city-id="17">اقليد</option>
                                            <option value="234" data-city-id="17">اوز</option>
                                            <option value="235" data-city-id="17">ایج</option>
                                            <option value="236" data-city-id="17">ایزد خواست</option>
                                            <option value="237" data-city-id="17">باب انار</option>
                                            <option value="238" data-city-id="17">بالاده</option>
                                            <option value="239" data-city-id="17">بنارويه</option>
                                            <option value="240" data-city-id="17">بهمن</option>
                                            <option value="241" data-city-id="17">بوانات</option>
                                            <option value="242" data-city-id="17">بيرم</option>
                                            <option value="243" data-city-id="17">بیضا</option>
                                            <option value="244" data-city-id="17">جنت شهر</option>
                                            <option value="245" data-city-id="17">جهرم</option>
                                            <option value="246" data-city-id="17">حاجي آباد-زرین دشت</option>
                                            <option value="247" data-city-id="17">خاوران</option>
                                            <option value="248" data-city-id="17">خرامه</option>
                                            <option value="249" data-city-id="17">خشت</option>
                                            <option value="250" data-city-id="17">خفر</option>
                                            <option value="251" data-city-id="17">خنج</option>
                                            <option value="252" data-city-id="17">خور</option>
                                            <option value="253" data-city-id="17">داراب</option>
                                            <option value="254" data-city-id="17">رونيز عليا</option>
                                            <option value="255" data-city-id="17">زاهدشهر</option>
                                            <option value="256" data-city-id="17">زرقان</option>
                                            <option value="257" data-city-id="17">سده</option>
                                            <option value="258" data-city-id="17">سروستان</option>
                                            <option value="259" data-city-id="17">سعادت شهر</option>
                                            <option value="260" data-city-id="17">سورمق</option>
                                            <option value="261" data-city-id="17">ششده</option>
                                            <option value="262" data-city-id="17">شيراز</option>
                                            <option value="263" data-city-id="17">صغاد</option>
                                            <option value="264" data-city-id="17">صفاشهر</option>
                                            <option value="265" data-city-id="17">علاء مرودشت</option>
                                            <option value="266" data-city-id="17">عنبر</option>
                                            <option value="267" data-city-id="17">فراشبند</option>
                                            <option value="268" data-city-id="17">فسا</option>
                                            <option value="269" data-city-id="17">فيروز آباد</option>
                                            <option value="270" data-city-id="17">قائميه</option>
                                            <option value="271" data-city-id="17">قادر آباد</option>
                                            <option value="272" data-city-id="17">قطب آباد</option>
                                            <option value="273" data-city-id="17">قير</option>
                                            <option value="274" data-city-id="17">كازرون</option>
                                            <option value="275" data-city-id="17">كنار تخته</option>
                                            <option value="276" data-city-id="17">گراش</option>
                                            <option value="277" data-city-id="17">لار</option>
                                            <option value="278" data-city-id="17">لامرد</option>
                                            <option value="279" data-city-id="17">لپوئی</option>
                                            <option value="280" data-city-id="17">لطيفي</option>
                                            <option value="281" data-city-id="17">مبارك آباد ديز</option>
                                            <option value="282" data-city-id="17">مرودشت</option>
                                            <option value="283" data-city-id="17">مشكان</option>
                                            <option value="284" data-city-id="17">مصير</option>
                                            <option value="285" data-city-id="17">مهر فارس(گله دار)</option>
                                            <option value="286" data-city-id="17">ميمند</option>
                                            <option value="287" data-city-id="17">نوبندگان</option>
                                            <option value="288" data-city-id="17">نودان</option>
                                            <option value="289" data-city-id="17">نورآباد</option>
                                            <option value="290" data-city-id="17">ني ريز</option>
                                            <option value="291" data-city-id="17">کوار</option>
                                            <option value="292" data-city-id="18">آبيك</option>
                                            <option value="293" data-city-id="18">البرز</option>
                                            <option value="294" data-city-id="18">بوئين زهرا</option>
                                            <option value="295" data-city-id="18">تاكستان</option>
                                            <option value="296" data-city-id="18">قزوين</option>
                                            <option value="297" data-city-id="18">محمود آباد نمونه</option>
                                            <option value="298" data-city-id="19">قم</option>
                                            <option value="299" data-city-id="20">بانه</option>
                                            <option value="300" data-city-id="20">بيجار</option>
                                            <option value="301" data-city-id="20">دهگلان</option>
                                            <option value="302" data-city-id="20">ديواندره</option>
                                            <option value="303" data-city-id="20">سقز</option>
                                            <option value="304" data-city-id="20">سنندج</option>
                                            <option value="305" data-city-id="20">قروه</option>
                                            <option value="306" data-city-id="20">كامياران</option>
                                            <option value="307" data-city-id="20">مريوان</option>
                                            <option value="308" data-city-id="21">بابك</option>
                                            <option value="309" data-city-id="21">بافت</option>
                                            <option value="310" data-city-id="21">بردسير</option>
                                            <option value="311" data-city-id="21">بم</option>
                                            <option value="312" data-city-id="21">جيرفت</option>
                                            <option value="313" data-city-id="21">راور</option>
                                            <option value="314" data-city-id="21">رفسنجان</option>
                                            <option value="315" data-city-id="21">زرند</option>
                                            <option value="316" data-city-id="21">سيرجان</option>
                                            <option value="317" data-city-id="21">كرمان</option>
                                            <option value="318" data-city-id="21">كهنوج</option>
                                            <option value="319" data-city-id="21">منوجان</option>
                                            <option value="320" data-city-id="22">اسلام آباد غرب</option>
                                            <option value="321" data-city-id="22">پاوه</option>
                                            <option value="322" data-city-id="22">تازه آباد- ثلاث باباجانی</option>
                                            <option value="323" data-city-id="22">جوانرود</option>
                                            <option value="324" data-city-id="22">سر پل ذهاب</option>
                                            <option value="325" data-city-id="22">سنقر كليائي</option>
                                            <option value="326" data-city-id="22">صحنه</option>
                                            <option value="327" data-city-id="22">قصر شيرين</option>
                                            <option value="328" data-city-id="22">كرمانشاه</option>
                                            <option value="329" data-city-id="22">كنگاور</option>
                                            <option value="330" data-city-id="22">گيلان غرب</option>
                                            <option value="331" data-city-id="22">هرسين</option>
                                            <option value="332" data-city-id="23">دهدشت</option>
                                            <option value="333" data-city-id="23">دوگنبدان</option>
                                            <option value="334" data-city-id="23">سي سخت- دنا</option>
                                            <option value="335" data-city-id="23">گچساران</option>
                                            <option value="336" data-city-id="23">ياسوج</option>
                                            <option value="337" data-city-id="24">آزاد شهر</option>
                                            <option value="338" data-city-id="24">آق قلا</option>
                                            <option value="339" data-city-id="24">انبارآلوم</option>
                                            <option value="340" data-city-id="24">اینچه برون</option>
                                            <option value="341" data-city-id="24">بندر گز</option>
                                            <option value="342" data-city-id="24">تركمن</option>
                                            <option value="343" data-city-id="24">جلین</option>
                                            <option value="344" data-city-id="24">خان ببین</option>
                                            <option value="345" data-city-id="24">راميان</option>
                                            <option value="346" data-city-id="24">سرخس کلاته</option>
                                            <option value="347" data-city-id="24">سیمین شهر</option>
                                            <option value="348" data-city-id="24">علي آباد كتول</option>
                                            <option value="349" data-city-id="24">فاضل آباد</option>
                                            <option value="350" data-city-id="24">كردكوي</option>
                                            <option value="351" data-city-id="24">كلاله</option>
                                            <option value="352" data-city-id="24">گالیکش</option>
                                            <option value="353" data-city-id="24">گرگان</option>
                                            <option value="354" data-city-id="24">گمیش تپه</option>
                                            <option value="355" data-city-id="24">گنبد كاووس</option>
                                            <option value="356" data-city-id="24">مراوه تپه</option>
                                            <option value="357" data-city-id="24">مينو دشت</option>
                                            <option value="358" data-city-id="24">نگین شهر</option>
                                            <option value="359" data-city-id="24">نوده خاندوز</option>
                                            <option value="360" data-city-id="24">نوکنده</option>
                                            <option value="361" data-city-id="25">آستارا</option>
                                            <option value="362" data-city-id="25">آستانه اشرفيه</option>
                                            <option value="363" data-city-id="25">املش</option>
                                            <option value="364" data-city-id="25">بندرانزلي</option>
                                            <option value="365" data-city-id="25">خمام</option>
                                            <option value="366" data-city-id="25">رشت</option>
                                            <option value="367" data-city-id="25">رضوان شهر</option>
                                            <option value="368" data-city-id="25">رود سر</option>
                                            <option value="369" data-city-id="25">رودبار</option>
                                            <option value="370" data-city-id="25">سياهكل</option>
                                            <option value="371" data-city-id="25">شفت</option>
                                            <option value="372" data-city-id="25">صومعه سرا</option>
                                            <option value="373" data-city-id="25">فومن</option>
                                            <option value="374" data-city-id="25">كلاچاي</option>
                                            <option value="375" data-city-id="25">لاهيجان</option>
                                            <option value="376" data-city-id="25">لنگرود</option>
                                            <option value="377" data-city-id="25">لوشان</option>
                                            <option value="378" data-city-id="25">ماسال</option>
                                            <option value="379" data-city-id="25">ماسوله</option>
                                            <option value="380" data-city-id="25">منجيل</option>
                                            <option value="381" data-city-id="25">هشتپر</option>
                                            <option value="382" data-city-id="26">ازنا</option>
                                            <option value="383" data-city-id="26">الشتر</option>
                                            <option value="384" data-city-id="26">اليگودرز</option>
                                            <option value="385" data-city-id="26">بروجرد</option>
                                            <option value="386" data-city-id="26">پلدختر</option>
                                            <option value="387" data-city-id="26">خرم آباد</option>
                                            <option value="388" data-city-id="26">دورود</option>
                                            <option value="389" data-city-id="26">سپید دشت</option>
                                            <option value="390" data-city-id="26">كوهدشت</option>
                                            <option value="391" data-city-id="26">نورآباد (خوزستان)</option>
                                            <option value="392" data-city-id="27">آمل</option>
                                            <option value="393" data-city-id="27">بابل</option>
                                            <option value="394" data-city-id="27">بابلسر</option>
                                            <option value="395" data-city-id="27">بلده</option>
                                            <option value="396" data-city-id="27">بهشهر</option>
                                            <option value="397" data-city-id="27">پل سفيد</option>
                                            <option value="398" data-city-id="27">تنكابن</option>
                                            <option value="399" data-city-id="27">جويبار</option>
                                            <option value="400" data-city-id="27">چالوس</option>
                                            <option value="401" data-city-id="27">خرم آباد</option>
                                            <option value="402" data-city-id="27">رامسر</option>
                                            <option value="403" data-city-id="27">رستم کلا</option>
                                            <option value="404" data-city-id="27">ساري</option>
                                            <option value="405" data-city-id="27">سلمانشهر</option>
                                            <option value="406" data-city-id="27">سواد كوه</option>
                                            <option value="407" data-city-id="27">فريدون كنار</option>
                                            <option value="408" data-city-id="27">قائم شهر</option>
                                            <option value="409" data-city-id="27">گلوگاه</option>
                                            <option value="410" data-city-id="27">محمودآباد</option>
                                            <option value="411" data-city-id="27">مرزن آباد</option>
                                            <option value="412" data-city-id="27">نكا</option>
                                            <option value="413" data-city-id="27">نور</option>
                                            <option value="414" data-city-id="27">نوشهر</option>
                                            <option value="415" data-city-id="28">آشتيان</option>
                                            <option value="416" data-city-id="28">اراك</option>
                                            <option value="417" data-city-id="28">تفرش</option>
                                            <option value="418" data-city-id="28">خمين</option>
                                            <option value="419" data-city-id="28">دليجان</option>
                                            <option value="420" data-city-id="28">ساوه</option>
                                            <option value="421" data-city-id="28">شازند</option>
                                            <option value="422" data-city-id="28">محلات</option>
                                            <option value="423" data-city-id="28">کمیجان</option>
                                            <option value="424" data-city-id="29">ابوموسي</option>
                                            <option value="425" data-city-id="29">انگهران</option>
                                            <option value="426" data-city-id="29">بستك</option>
                                            <option value="427" data-city-id="29">بندر جاسك</option>
                                            <option value="428" data-city-id="29">بندر لنگه</option>
                                            <option value="429" data-city-id="29">بندرعباس</option>
                                            <option value="430" data-city-id="29">پارسیان</option>
                                            <option value="431" data-city-id="29">حاجي آباد</option>
                                            <option value="432" data-city-id="29">دشتی</option>
                                            <option value="433" data-city-id="29">دهبارز (رودان)</option>
                                            <option value="434" data-city-id="29">قشم</option>
                                            <option selected value="435" data-city-id="29">كيش</option>
                                            <option value="436" data-city-id="29">ميناب</option>
                                            <option value="437" data-city-id="30">اسدآباد</option>
                                            <option value="438" data-city-id="30">بهار</option>
                                            <option value="439" data-city-id="30">تويسركان</option>
                                            <option value="440" data-city-id="30">رزن</option>
                                            <option value="441" data-city-id="30">كبودر اهنگ</option>
                                            <option value="442" data-city-id="30">ملاير</option>
                                            <option value="443" data-city-id="30">نهاوند</option>
                                            <option value="444" data-city-id="30">همدان</option>
                                            <option value="445" data-city-id="31">ابركوه</option>
                                            <option value="446" data-city-id="31">اردكان</option>
                                            <option value="447" data-city-id="31">اشكذر</option>
                                            <option value="448" data-city-id="31">بافق</option>
                                            <option value="449" data-city-id="31">تفت</option>
                                            <option value="450" data-city-id="31">مهريز</option>
                                            <option value="451" data-city-id="31">ميبد</option>
                                            <option value="452" data-city-id="31">هرات</option>
                                            <option value="453" data-city-id="31">يزد</option>
                                        </select>
                                    </div>
                                   <div class="form-group col-md-6">
                                        <label for="city_id" class="sr-only"> استان :</label>
                                        <select class="form-control input-sm" required name="city_id">
                                            <option value="">- استان -</option>
                                            <option value="1">آذربايجان شرقي</option>
                                            <option value="2">آذربايجان غربي</option>
                                            <option value="3">اردبيل</option>
                                            <option value="4">اصفهان</option>
                                            <option value="5">البرز</option>
                                            <option value="6">ايلام</option>
                                            <option value="7">بوشهر</option>
                                            <option value="8">تهران</option>
                                            <option value="9">چهارمحال بختياري</option>
                                            <option value="10">خراسان جنوبي</option>
                                            <option value="11">خراسان رضوي</option>
                                            <option value="12">خراسان شمالي</option>
                                            <option value="13">خوزستان</option>
                                            <option value="14">زنجان</option>
                                            <option value="15">سمنان</option>
                                            <option value="16">سيستان و بلوچستان</option>
                                            <option value="17">فارس</option>
                                            <option value="18">قزوين</option>
                                            <option value="19">قم</option>
                                            <option value="20">كردستان</option>
                                            <option value="21">كرمان</option>
                                            <option value="22">كرمانشاه</option>
                                            <option value="23">كهكيلويه و بويراحمد</option>
                                            <option value="24">گلستان</option>
                                            <option value="25">گيلان</option>
                                            <option value="26">لرستان</option>
                                            <option value="27">مازندران</option>
                                            <option value="28">مركزي</option>
                                            <option selected value="29">هرمزگان</option>
                                            <option value="30">همدان</option>
                                            <option value="31">يزد</option>
                                            <option value="31">يزد</option>
                                        </select>
                                    </div>
                               </div>
                                <div class="row form-inline-custom">
                                    <div class="form-group col-md-6">
                                        <label for="area_id" class="sr-only">منطقه </label>
                                        <select required class="form-control input-sm" name="area_id">
                                            <option value="">منطقه</option>
                                            <option value="1">یک</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="zone_id" class="sr-only">ناحیه </label>
                                        <select required class="form-control input-sm" name="zone_id">
                                            <option value=""> ناحیه </option>
                                            <option value="1">5</option>
                                        </select>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                        <div class="form-group text-center">

                            <button id="sendBtn" type="submit" class="btn btn-primary">ارسال</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--end modal -->

    <!-- ourBody END -->
    <!-- Our Default modal template - END -->

    <!-- START Sripts -->
    <script type="text/javascript" src="{static_path}/lib/jquery.min.js"></script>
    <script type="text/javascript" src="{static_path}/js/cleanzone.js"></script>
    <script src="{static_path}/lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="{static_path}/lib/jquery.gritter/jquery.gritter.min.js" type="text/javascript"></script>
    <script src="{static_path}/lib/jquery.icheck/icheck.min.js" type="text/javascript"></script>
    <script src="{static_path}/lib/jquery.parsley/parsley.remote.min.js" type="text/javascript"></script>
    <script src="{static_path}/lib/jquery.parsley/parsley.config.js" type="text/javascript"></script>
	 <script src="{static_path}/js/tmpl.min.js" type="text/javascript"></script>
	 <script type="text/javascript">
	 
		if ({show_register})
		$('#win-studentRegister').modal({
			show: true,
			backdrop: 'static'
		});
		
		var current_session = {current_session};
		
		var packID = 1;
		
		var reputation = "{reputation}";
		
		var medals = {medals};
		
		var timeline = {timeline};
		
	</script>
    <script src="{static_path}/js/forms.js" type="text/javascript"></script>
    <script src="{static_path}/js/student.js" type="text/javascript"></script>
    <script type="text/javascript">
        App.init();
    </script>
    <!-- END Sripts -->
</body>

</html>