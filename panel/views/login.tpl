﻿<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ورود</title>
    <link href="{static_path}/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{static_path}/lib/font-awesome/css/font-awesome.min.css">
    <!--if lt IE 9script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
    -->
    <link href="{static_path}/css/style.css" rel="stylesheet">
	<style>
	.content{
		direction:ltr;
	}
	
	.title{
		color:red;
	}
	</style>
  </head>
  <body class="texture">
    <div id="cl-wrapper" class="login-container">
      <div class="middle-login">
        <div class="block-flat">
          <div class="header">
            <h3 class="text-center">ورود</h3>
          </div>
          <div>
            <form style="margin-bottom: 0px !important;" action="" method="post" class="form-horizontal">
              <div class="content">
                <h4 class="title">{message}</h4>
                <div class="form-group">
                  <div class="col-sm-12">
                    <div class="input-group"><span class="input-group-addon"><i class="fa fa-user"></i></span>
                      <input name="username" type="text" placeholder="نام کاربری" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                    <div class="input-group"><span class="input-group-addon"><i class="fa fa-lock"></i></span>
                      <input name="password" type="password" placeholder="رمزعبور" class="form-control">
                    </div>
                  </div>
                </div>
              </div>
              <div class="foot">
                <button data-dismiss="modal" type="submit" name="login" class="btn btn-primary">ورود</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
	<script type="text/javascript" src="{static_path}/lib/jquery.min.js"></script>
	<script type="text/javascript" src="{static_path}/js/cleanzone.js"></script>
	<script src="{static_path}/lib/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
      	//initialize the javascript
      	App.init();
      });
      
    </script>
  </body>
</html>