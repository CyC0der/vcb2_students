<?php
/**
 *
 * PHP version 5.3
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @author     korosh raoufi <k2_4u@yahoo.com>
 * @version    Release: v1.0
 * @link       http://k2-4u.com
 *
 */


/** database configure **/
const DB_SERVER = 'localhost';
const DB_NAME =	'vcb2';
const DB_USER =	'root';
const DB_PASS =	'';

function __autoload($class_name)
{
	$classInfo = explode('\\', $class_name);
	
	switch ($classInfo [0]) {
		case
			'controller':
			$path =  '/' . SITE_ROOT . '/controller/';
			
			if (!file_exists(SYSTEM_PATH . '/' . $path . $classInfo[1] . '.php')) {
				header('location: 404.html');
				exit;
			};
			
			break;
			
		case
			'model':
			$path = '/' . SITE_ROOT . '/model/';
			break;
			
		case
			'library':
			$path = '/core/library/';
			break;
		
		case
			'engine':
			$path = '/core/engine/';
			break;
			
		default:
			return false;
	};
	
	$file = SYSTEM_PATH . $path . $classInfo[1] . '.php';

    if (!file_exists($file))
        return false;

	require_once $file;
};


?>