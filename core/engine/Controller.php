<?php
/**
 *
 * PHP version 5.3
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @author     korosh raoufi <k2_4u@yahoo.com>
 * @version    Release: v1.0
 * @link       http://k2-4u.com
 *
 */
 
namespace engine;

Abstract Class Controller
{
	protected $registry;

	function __construct($registry, $model)
	{
		$this->registry = $registry;
		$this->model = $model;
	}

	public function __get($key) {
		return $this->registry->get($key);
	}
	
	public function __set($key, $value) {
		$this->registry->set($key, $value);
	}
	
	abstract function index();
};

?>