<?php

//require_once dirname(__FILE__) . '/jdatetime.class.php';

const REGEX_TELL  =  '/^[0-9]{8}$/i';
const REGEX_POSTALCODE = '/^[0-9]{10}$/i';
const REGEX_CELL =  '/^(09[0-9]{9})$/i';
const REGEX_SERIAL = '/^[0-9]{12}$/i';

const VLD_STRIP_HTML =  1;
const VLD_ENCODE_HTML = 2;

const VLD_LEN_FIRSTNAME = 30;
const VLD_LEN_LASTNAME = 40;

class validation{
	
	public $vFuncFactory = array();
	public $message;
	
	function __construct($numberOfVars,$MySQL,$message = false){
		
		if($message == false)
			$message = new message();
		
		$this->message = $message;
			
		$this->numberOfVars = $numberOfVars;
		$this->MySQL = $MySQL;
	}
	
	public function post($name){
		
		$vFunc = new validation_function($_POST[$name],$name);	
		
		$vFunc->message = $this->message;
		
		$this->vFuncFactory[] = $vFunc;
		
		return $vFunc;
	}
	
	public function get($name){
		
		$vFunc = new validation_function($_GET[$name],$name);
		
		$this->vFuncFactory[] = $vFunc;
		
		return $vFunc;
	}
	
	public function check(){

		foreach($this->vFuncFactory as $vFunc)
		if($vFunc->validateResult() == false) {
			return false;
		}

		if($this->numberOfVars != count($this->vFuncFactory)) {
			
			$this->message->add(array(
				'status'=>0,
				'type'=>'danger',
				'text'=>'خطا، تعداد مقادیر ورودی غیرمجاز است'
			));
			
			return false;
		}
		
		return true;
	}

	public function values() {
		$out = array();
		foreach($this->vFuncFactory as $vFunc)
			//$out[$vFunc->name()] = $this->MySQL->real_escape_string($vFunc->value());
			$out[$vFunc->name()] = $vFunc->value();
			
		return $out;
	}

}

class validation_function{
	
	private $val;
	private $true_flag = true;
	private $name;
	public $message;
	
	function __construct($val,$name) {
		$this->val = $val;
		$this->name = $name;
	}
	
	public function required(){

		$result = !empty($this->val);
		
		if($result === false) {
			$this->true_flag = false;
			$this->message->add(XHHTP_REQIURED);
		}
		
		return $this;
	}
		
	public function checkbox(){
		
		$this->val = (filter_var($this->val, FILTER_VALIDATE_BOOLEAN) ? 1 : 0 );
				
		$this->true_flag = true;
		return $this;
	}
	
	public function pattern($pattern_const){
		
		$result = (boolean)preg_match($pattern_const,$this->val,$out);
		
		$this->val = $out[0];
		
		if($result === false) {
			$this->message->add(array(
				'status'=>0,
				'type'=>'danger',
				'text'=>'خطا در مقادیر ورودی ، یکی از فیلد ها خارج از چهارچوب تعیین شده است'
			));
			$this->true_flag = false;
		}

		return $this;
	}
	
	public function email(){
		
		$this->val = filter_var($this->val, FILTER_VALIDATE_EMAIL);
		
		if($this->val === false) {
			$this->message->add(array(
				'status'=>0,
				'type'=>'danger',
				'text'=>'خطا،  آدرس ایمیل اشتباه است'
			));
			$this->true_flag = false;
		}

		return $this;
	}
	
	public function enum($max){
		
		$this->val = filter_var($this->val, FILTER_VALIDATE_INT, 
			array('options' => 
				array('min_range' => 1,'max_range' => $max)
		));
		
		if($this->val === false) {
			$this->message->add(array(
				'status'=>0,
				'type'=>'danger',
				'text'=>'خطا در مقادیر ورودی ، رادیو باکس انتخاب نشده و یا مقادیر اشتباه در لیست ها'
			));
			$this->true_flag = false;
		}

		return $this;
	}
	
	public function between($start,$end){
		
		$result = filter_var($this->val, FILTER_VALIDATE_INT, 
			array('options' => 
				array('min_range' => $start,'max_range' => $end)
		));
		
		if($result === false) {
			$this->message->add(array(
				'status'=>0,
				'type'=>'danger',
				'text'=>'خطا در مقادیر ورودی ، ، مقدار یک فیلد عددی خارج از محدوده مجاز است'
			));
			$this->true_flag = false;
		}

		return $this;
	}
	
	public function idCheck($tableName,$where=false,$sql=false,$callback=false){
		
		$result = true;
		
		//$this->message->add(XHHTP_DB_FAULT);
		
		if($result === false) {
			$this->message->add(XHHTP_INPUT_INVALID);
			$this->true_flag = false;
		}
		
		return $this;
	}
	
	public function date($min=false,$max=false){
		
		$val_is_date = (boolean)preg_match("@^([0-9]{4}[-/][0-9]{1,2}[-/][0-9]{1,2})$@",$this->val,$out);
		
		$val = strtotime($out[0]);
		 
		//$this->val =  date('m/d/Y', $val);  its not work for persian date (returns false)
		
		if($min && $max) {
			
			$min = strtotime($min);
			$max = strtotime($max);
			$result = ($val_is_date && ($val!==false) && ($val >= $min) && ($val <= $max));
			
		}else if($max) {
			
			 $max = strtotime($max);
			 $result = ($val_is_date && ($val!==false) && $val <= $max);
			
		}else if($min){
			
			 $min = strtotime($min);
			 $result = ($val_is_date && ($val!==false) && $val >= $min);
			
		}else{
			//$result = $val_is_date && ($val!==false);
			$result = (boolean)$val_is_date;
		}
		
		if($result === false) {
			$this->message->add(array(
				'status'=>0,
				'type'=>'danger',
				'text'=>'خطا از مقادیر ورودی ،  تاریخ اشتباه انتخاب شده'
			));
			$this->true_flag = false;
		}
		
		return $this;
	}
		
	public function persianOnly($include_number = false,$include_en_number = false){
		
		$extra='';
		
		if($include_number)
			$extra += '۱۲۳۴۵۶۷۸۹۰';
		
		if($include_en_number)
			$extra += '0-9';
		
		$result =  (boolean)preg_match('/^['.$extra.'آپچجحخهعغفقثصضشسیبلاتنمکگوئدذرزطظژؤإأءًٌٍَُِّ\s]+$/u',$this->val,$out);
		
		$this->val = $out[0];

		if($result === false) {
			$this->message->add(array(
				'status'=>0,
				'type'=>'danger',
				'text'=>'خطا از مقادیر وروردی ، در فیلد محدود به حروف  فارسی ، از حروف به غیر از فارسی استفاده شده'
			));
			$this->true_flag = false;
		}
		
		return $this;
	}

	public function text($maxChar,$scapeMode = VLD_STRIP_HTML){ /* Strip HTML or Encode HTML or nothing */
	
		$len = mb_strlen($this->val,'utf-8');
		
		switch($scapeMode) {
			case VLD_STRIP_HTML:
				$this->val = strip_tags($this->val);
			break;
			case VLD_ENCODE_HTML:
				$this->val = htmlspecialchars($this->val, ENT_QUOTES,"UTF-8");
			break;
		}
		
		$result  = ($len <= $maxChar);
		
		if($result === false) {
			$this->message->add(array(
				'status'=>0,
				'type'=>'danger',
				'text'=>'خطا از مقادیر وروردی ، تعداد حروف یکی از ورودی ها بیشتر از حد مجاز تعیین شده است'
			));
			$this->true_flag = false;
		}
		
		return $this;
	}

	public function value(){
		return $this->val;
	}
	
	public function name(){
		return $this->name;
	}
	
	function __toString() {
		return $this->val;
	}
	
	public function validateResult() {
		return $this->true_flag;
	}

}

?>