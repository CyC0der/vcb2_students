<?php
/**
 *
 * PHP version 5.3
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @author     korosh raoufi <k2_4u@yahoo.com>
 * @version    Release: v1.0
 * @link       http://k2-4u.com
 *
 */
 
namespace library;

require_once SYSTEM_PATH . '/core/library/template_func.php';

class Template extends templateFunction
{

	private $tags = array();
	private $required_tags = array();
	private $blocks = array();
	private $group_block = array();
	private $tpl = NULL;
	private $parsed_tpl = NULL;
	protected $template_fileType = NULL;

	function __construct($template_fileType = 'tpl')
	{
		$this->template_fileType = $template_fileType;
	}
	
	/** add variable to the template **/
	public function assign($input, $value = NULL, $required = FALSE)
	{
		if (is_array($input)) {
			foreach ($input as $tag => $value) {
				if (empty($tag))
					throw new Exception('Template::assign() empty argement 1.');
				
				if ($required == TRUE)
					$this->required_tags[$tag] = $value;
				else
					$this->tags[$tag] = $value;
			};
		} elseif (is_string($input)) {
			if (empty($input)) {
				throw new Exception('Template::assign() empty argement 1.');
			} else {
				if ($required == TRUE)
					$this->required_tags[$input] = $value;
				else
					$this->tags[$input] = $value;
			};
		} else {
			return FALSE;
		};
		return TRUE;
	}

	/** add block to the template **/
	public function add_block($block_name, $block_array)
	{
		if (!is_string($block_name) || empty($block_name))
			throw new Exception('Template::add_block() argement 1 Block name is not a string or is empty.');
		
		if (!is_array($block_array))
			throw new Exception('Template::add_block() argement 2 Block array is not an array.');
			
		
		$group = FALSE;
		
		if (preg_match('/^(.*):(.*)$/i',$block_name,$parts)) {
		
			$this->group_block[$parts[1]][$parts[2]] = TRUE;
			
			$block_name = $parts[1];
			
			$group = $parts[2];

		};
			

		$this->blocks[$block_name][] = array(
			'data' => $block_array,
			'group' => $group
		);
	}
	
	/** load a template with name **/
	public function load($name)
	{
		if(empty($name))
			throw new \Exception('Template::loadTemplate() empty template name!.');

		$template = file_get_contents(SYSTEM_PATH . '/' . SITE_ROOT .'/views/' . $name . '.' . $this->template_fileType);
		
		if ($template == FALSE)
			throw new \Exception('Template::load_file() can\'t open file.');

		$this->tpl .= $template;
	}
	
	/** load template from string **/
	public function loadFromString($template)
	{
		$this->tpl .= $template;
	}
	
	/** clear all template variable **/
	public function free()
	{
		$this->tpl = NILL;
		$this->parsed_tpl = NULL;
		$this->tags = array();
		$this->required_tags = array();
		$this->blocks = array();
	}

	/** print template to the browser **/
	public function exec()
	{
		if (!empty($this->tpl))
			$this->parse();

		echo $this->parsed_tpl;
	}
	
	public function greenMessage($message)
	{
		$this->add_block('green_message', array('message'=>$message));
	}
	
	public function redMessage($message)
	{
		if (is_array($message)) {
		
			$this->add_block('red_message', 
				array('message'=>
					'<img src="static/images/fault.png" />'
					. join('<br><img src="static/images/fault.png" />', $message)
				)
			);
			
		} else {
		
			$this->add_block('red_message', array('message'=>$message));
		};
	}
	
	function __toString()
	{
		if (!empty($this->tpl))
			$this->parse();

		return $this->parsed_tpl;
	}

	private function functions()
	{
		$methods = get_class_methods('library\templateFunction');
		foreach($methods as $func) {
			$itemNums = preg_match_all('/\{' . $func . ':(.*)\}/', $this->tpl, $matches);
			if ($itemNums) {
				for ($i = 0; $i < $itemNums; $i++) {

					$this->tpl = str_replace('{'. $func .':' . $matches[1][$i] . '}', $this->{$func}($matches[1][$i]), $this->tpl);
				};
			};
		};
	}
	
	public function HTMLForm($formName, $request)
	{
		if (preg_match('@(<form[^><]+name="' . $formName . '"[^><]+>.*</form>)@sU', $this->tpl, $forms)) {
			
			$input = '<input[^><]+name=".*"[^><]*>';
			$select = '<select[^><]+name=".*"[^><]*>.*</select>';
			$textarea = '<textarea[^><]+name=".*"[^><]*>.*</textarea>';

			/** pars and get elements **/
			if (preg_match_all('@(' . $input . ')|(' . $select . ')|(' . $textarea . ')@sU', $forms[0], $elements)) {
			
				foreach ($elements[0] as $element):

					/** get elements Name **/
					if (preg_match('@<(input|select|textarea)[^><]*name="([A-Za-z0-9_\[\]]*)"[^><]*>@sU', $element, $name)) {
						$type = $name[1];
						$name = $name[2];
					};

					/** replace value to new value */

					switch ($type):
					case 
						'input':
							preg_match('@<input[^><]*type="(.*)"[^><]*>@sU', $element, $input_type);
							switch ($input_type[1]):
							
								case
									'text':
									if (preg_match('@<input[^><]*value="(.*)"[^><]*>@sU', $element, $value)) {
						
										$new_element = str_replace('value="' . $value[1] . '"', 
											'value="' . htmlspecialchars($request[$name]) . '"',
											$element
										);
							
									} else {
						
										$new_element = str_replace('<input', 
											'<input value="' . htmlspecialchars($request[$name]) . '"',
											$element
										);
									};
									break;
									
								case 
									'checkbox':
									if(isset($request[$name]) && $request[$name] == 1) {
										$new_element = str_replace('<input', 
											'<input checked="checked"',
											$element
										);
									}else {
										$new_element = str_replace('checked="checked"', '',
											$element
										);
									};
									break;
									
								default:
									$new_element = $element;
									break;
									
							endswitch;
						break;

					case
						'textarea':
						if (preg_match('@(<textarea[^><]*>)(.*)</textarea>@sU', $element, $value)) {

							$new_element = str_replace($value[1] . $value[2] . '</textarea>', 
								$value[1] . $request[$name] . '</textarea>',
								$element
							);
							
						};
						break;
						
					case
						'select':
						/*if (preg_match('@<select[^><]*>.*<option([^><]*selected.*)>[^<^>]*</option>.*</select>@sU', $element, $value)) {
							preg_match('@[^><]*value="(.*)"[^><]*@sU', $value[0], $value);
						};*/
							$new_element = $element;
						break;
						
					default:
						throw new \Exception(':D');
					break;
					endswitch;

					$this->tpl = str_replace($element, $new_element, $this->tpl);

				endforeach;
			};

		};
	}

	private function parse()
	{
		if (empty($this->tpl))
			return;
	
		# run template functions
		$this->functions();

		# blocks
		$tmp_blocknames = array();
		
		

		foreach ($this->blocks as $block_name => $block_arrays) {

			if (isset($this->group_block[$block_name]))
				$this->groupBlock($block_name);
			

			$part1 	 = preg_quote($block_name, '/');
			$part2 	 = preg_quote($block_name, '/');
			$express = '@<!--' . $part1 . '-->(.*)<!--/' . $part2 . '-->@sU';
		
			$itemNums = preg_match_all($express, $this->tpl, $matches);

			if ($itemNums) {
				for ($i = 0;$i < $itemNums; $i++) {

					$block_plus_definition = $matches[0][$i];
					$block = $matches[1][$i];

					if (is_int(strpos($block, '<!-- IF')))
						$parse_control_structures = TRUE;

					$parsed_block = NULL;

					foreach ($block_arrays as $block_array) {
					
						/* group block or normall */
						$tmp = (isset($this->group_block[$block_name]) ? $this->group_block[$block_name][$block_array[group]] : $block );
						
						$block_array = $block_array[data];	
						
						if (isset($parse_control_structures))
							$tmp = $this->_parse_control_structures($tmp, 
								array_merge($block_array, $this->tags, $this->required_tags)
							);

						foreach ($block_array as $tag_name => $tag_value)
							$tmp = str_replace('{' . $tag_name . '}', $tag_value, $tmp);

						$parsed_block .= $tmp;
					};
					
					
					$this->tpl = str_replace($block_plus_definition, $parsed_block, $this->tpl);
					
					$tmp_blocknames[] = $block_name;
					unset($parse_control_structures);
				};
			};
		};
		
		if (count($this->blocks) > 0)
			$this->tpl = preg_replace("@<!--(/?)(" . implode('|', $tmp_blocknames) . ")-->@", '', $this->tpl);

		# unbenutze blِcke entfernen
		$this->tpl = preg_replace("@<!--([a-zA-Z0-9:_-]+)-->.*<!--/\\1-->(\r\n|\r|\n)?@msU", '', $this->tpl);

		# single tags
		foreach ($this->required_tags as $tag_name => $tag_value) {
		
			if (!is_int(strpos($this->tpl, $tag_name)))
				throw new Exception('Template::parse() Could not find tag <i>'.$tag_name.'</i> in the template file!');

			$this->tpl = str_replace( '{'.$tag_name.'}', $tag_value, $this->tpl);
		}
		
		foreach ($this->tags as $tag_name => $tag_value)
			$this->tpl = str_replace('{'.$tag_name.'}', $tag_value, $this->tpl);

		# if & else
		$this->tpl = $this->_parse_control_structures(
			$this->tpl,
			array_merge($this->tags, $this->required_tags),
			$this->blocks
		);

		$this->parsed_tpl = $this->tpl;
		$this->tpl = '';
	}

	private function _parse_control_structures($tpl, $vars, $blocks = array())
	{
		$express  = '/<!-- IF (!?)((BLOCK )?)([_a-zA-Z0-9\-]+) -->(.*)((<!-- ELSEIF !\(\\1\\2\\4\) -->)(.*))?<!-- ENDIF \\1\\2\\4 -->/msU';
		
		$matchnumber = preg_match_all($express, $tpl, $matches);

		if($matchnumber) {
			for ($i = 0; $i < $matchnumber; $i++) {
				if( !empty( $matches[2][$i] ) )
				{
					$code = 'if( '.$matches[1][$i].'isset($blocks[\''.$matches[4][$i].'\']) )'."\n";
				}
				else
				{
					$code = 'if( '.$matches[1][$i].'( isset($vars[\''.$matches[4][$i].'\']) ) )'."\n";
				}
				$code .= '{ $tpl = str_replace( $matches[0][$i], $this->_parse_control_structures( $matches[5][$i], $vars, $blocks ), $tpl ); }'."\n";
				$code .= ' else '."\n";
				$code .= '{ $tpl = str_replace( $matches[0][$i], !empty($matches[7][$i]) ? $this->_parse_control_structures( $matches[8][$i], $vars, $blocks ) : \'\', $tpl ); }';
				eval( $code );
			}
		}         
		return $tpl;
	}
	
	private function groupBlock($block_name) 
	{
		$groups = array_keys($this->group_block[$block_name]);
			
		$gNum = count($groups);
			
		$i = 1;
			
		foreach($groups as $group) {
			
			$part1 	 = preg_quote($block_name . ':' . $group, '/');
			$part2 	 = preg_quote($block_name.  ':' . $group, '/');
			$express = '@<!--' . $part1 . '-->(.*)<!--/' . $part2 . '-->@sU';
					
			if (preg_match($express, $this->tpl, $matches)) {
				
				$this->group_block[$block_name][$group] = $matches[1];

				$replaced  = ($gNum == $i ? '<!--' . $block_name . '-->...<!--/' . $block_name . '-->' : '');
						
				$this->tpl = str_replace( $matches[0], $replaced, $this->tpl);
			};
				
			$i++;
		};
	}
}

?>
